# Ez bot

An easily configurable framework to create discord bot with less boilerplate code.

It's a wrapper around JDA (Java Discord API) to automate some of the recuring steps when developping a discord bot.

## Key features

* Working music bot (at least via youtube)
* Create commands by just inheriting a Class;
  support [discord slash command](https://support.discord.com/hc/en-us/articles/1500000368501-Slash-Commands-FAQ) but
  also pure text chat commands with parameters parser
* Create a lazy-command channel to execute some commands by reacting to an embed
* Personalize your bot text by overiding the i18n provider (built in support for
  a [simpllocalize.io](http://simpllocalize.io) project; but you can also write your own provider in 5 minutes)
* Help commands for all of your commands generated from your code.
* Other otpional default commands like status, ect
* Execute code at startup before listening to traffic
* Set the bot presence to the help command; this is overridable to whatever you want via a configuration option
* optional monitoring via [Sentry.io](https://sentry.io/)

## How to use

1. Creat a discord app and get it's token via the [developper portal](https://discord.com/developers/applications)
2. Copy your discord bot token in the environement variable DISCORD_API_TOKEN
3. Create a `Main` class and call `Orchestrator.start()`
4. Run you `Main` class

This wil start the bot with the default configuration.

## Configuration

Before calling `Orchestrator.start()` you can override the default configuration with your own implementation of
a `Config` class. To do so just assign the static variable `ConfigHandler.config` to an instance of your implementation.
For more details on all the config options see [the dedicated page](https://gitlab.com/ez-bot-vba/ez-bot/-/wikis/config)
.

Some sensitive values should not be put directly in the code base and thus are passed by environment variables (like
credentials). For more on those, see [their dedicated wiki](https://gitlab.com/ez-bot-vba/ez-bot/-/wikis/env-config).

## Adding logic

Any logic is held inside what we will refer to as a `Process`. Every instances of a process has access to a JDA gateway
so he can interact with discord API. There is three main types of those Processes.

### Scheduled Process

It is basiacly a cron that can impersonate your bot.

Every implementation override:

* a `cron` attribute that represent the CRON expression to schedule that process
* a `logic()` function that will be called every time tis CRON is called; from their you can interact with discord API
  throught the Process gateway.

### Background process

It's a process that can register a list of EventListener class. An EventListener class is a sub-class of
JDA's `ListenerAdapter` and should override methods from it like `onMessageReceived` for example.

*I could have put all the method overrides directly in the BackgroundProcess instead of passing via a list of different
class. But I added this extra layer to segment the code better. I recommend that a BackgroundProcess is responsible for
one feature (or related set of features) only, and having a class for the listeners for each "block" of that feature.*

### Command

It's the abstract representation of a command a user can trigger in different ways.

Each command must have:

* a list of aliases (the first being the name)
* a list of parameter types (string, int, but also channel, user, ect)
* a description
* some logic

It can also override other values to restrict it's whitelisted guilds, channels, roles and/or users

For more details on the commands see [the dedicated wiki](https://gitlab.com/ez-bot-vba/ez-bot/-/wikis/commands).

## Donation

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/nindouja)
