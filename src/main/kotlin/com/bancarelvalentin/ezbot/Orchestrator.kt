package com.bancarelvalentin.ezbot

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.default.commands.dj.DjClearQueueCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjConnectCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjCurrentlyPlayingCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjLoopbackCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjNextCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjPreviousCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjQueueCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjStopCommand
import com.bancarelvalentin.ezbot.default.commands.dj.DjToggleCommand
import com.bancarelvalentin.ezbot.default.commands.help.HelpTextCommand
import com.bancarelvalentin.ezbot.default.commands.reseti18n.ResetI18nCommand
import com.bancarelvalentin.ezbot.default.commands.status.StatusTextCommand
import com.bancarelvalentin.ezbot.default.features.chatcommands.ChatCommandBot
import com.bancarelvalentin.ezbot.default.features.dj.DjDedicatedChannelPlayerBot
import com.bancarelvalentin.ezbot.default.features.lazycommands.LazyCommandsBot
import com.bancarelvalentin.ezbot.default.features.liveconfig.LiveConfigHandlerBot
import com.bancarelvalentin.ezbot.default.features.slashcommands.SlashCommandBot
import com.bancarelvalentin.ezbot.exception.implerrors.conf.MissingDefaultLiveConfig
import com.bancarelvalentin.ezbot.exception.implerrors.orchestrator.CommandNotFoundException
import com.bancarelvalentin.ezbot.exception.implerrors.orchestrator.ProcessNotFoundException
import com.bancarelvalentin.ezbot.exception.implerrors.startup.BotAlreadyStartedException
import com.bancarelvalentin.ezbot.exception.implerrors.startup.CantSupportAnyOtherCommandsTypeWithoutChatCommands
import com.bancarelvalentin.ezbot.exception.implerrors.startup.MissingDjChannelId
import com.bancarelvalentin.ezbot.exception.implerrors.startup.MissingLazyCommandChannelId
import com.bancarelvalentin.ezbot.exception.implerrors.startup.MissingSentryDSN
import com.bancarelvalentin.ezbot.exception.implerrors.startup.MissingSimpleLocalizedApiKey
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.process.EventListenerProcess
import com.bancarelvalentin.ezbot.process.Instance
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.scheduled.ScheduledProcess
import com.bancarelvalentin.ezbot.utils.SentryUtils
import io.sentry.Sentry
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.events.ReadyEvent
import java.util.function.Function
import java.util.stream.Collectors

/**
 * Invite the bot: https://discord.com/oauth2/authorize?client_id=$$$$$&scope=bot&permissions=8
 */

@Suppress("unused")
class Orchestrator {

    companion object {

        private val logger = Logger(Orchestrator::class.java)

        lateinit var gateway: JDA
        private val builder = JDABuilder.createDefault(EnvConfig.DISCORD_API_TOKEN)
        private val processes = ArrayList<Process>()
        private val commands = ArrayList<Command>()
        var started = false
            private set
        var running = false
            private set
        var liveConfigReady = false
            private set

        private val startedListeners = ArrayList<Runnable>()
        private val readyListeners = ArrayList<Runnable>()
        private val liveConfigListeners = ArrayList<Runnable>()

        fun add(vararg process: Process) {
            processes.addAll(process)
        }

        fun add(vararg command: Command) {
            commands.addAll(command)
        }

        fun add(vararg instances: Instance) {
            for (instance in instances)
                when (instance) {
                    is Process -> add(instance)
                    is Command -> add(instance)
                }
        }

        fun listenForStart(runnable: Runnable) {
            startedListeners.add(runnable)
        }

        fun listenForReady(runnable: Runnable) {
            readyListeners.add(runnable)
        }

        fun listenForLiveConfig(runnable: Runnable) {
            readyListeners.add(runnable)
        }

        fun liveConfigReady() {
            if (!liveConfigReady) readyListeners.forEach { it.run() }
            liveConfigReady = true
        }

        fun start() {
            if (started) {
                throw BotAlreadyStartedException()
            }
            try {
                if (ConfigHandler.config.logErrorsOnSentry || ConfigHandler.config.timeCommandExecutionsInSentry || ConfigHandler.config.timeCronExecutionsInSentry) {
                    if (ConfigHandler.config.sentryDsn != null) {
                        logger.info("Activated sentryDsn")
                        Sentry.init { options ->
                            options.dsn = ConfigHandler.config.sentryDsn
                            options.environment = if (EnvConfig.DEV) "dev" else "prod"
                            options.release = ConfigHandler.config.version
                            options.tracesSampleRate = 1.0
                            options.setDebug(EnvConfig.DEV)
                        }
                    } else {
                        throw MissingSentryDSN()
                    }
                }
                val transaction = SentryUtils.createPerf("[START] initial startup", ConfigHandler.config.timeStartupInSentry, null)
                activateFeatures()

                logger.info("====================", LogCategoriesEnum.INITIAL_STARTUP)
                logger.info("Starting...", LogCategoriesEnum.INITIAL_STARTUP)
                logger.info("Version: ${ConfigHandler.config.version ?: "(version not set)"}", LogCategoriesEnum.INITIAL_STARTUP)
                logger.info("Prefixes: ${ConfigHandler.config.prefixes.toList().joinToString(" \t ")}", LogCategoriesEnum.INITIAL_STARTUP)
                processes.forEach {
                    if (it is EventListenerProcess && it.realReadyListeners == null && it.getListeners().isEmpty()) {
                        logger.warn("Useless EventListenerProcess ${this::class.java.simpleName}")
                    }
                    when (it) {
                        is ScheduledProcess -> it.registerCron()
                        is BackgroundProcess -> builder.addEventListeners(*(it as EventListenerProcess).getListeners())
                    }
                }

                builder.addEventListeners(OrchestratorOnReadyFinishStart())
                gateway = builder.build() // Build gateway
                startedListeners.forEach { it.run() }

                logger.info("Started; waiting for post ready to complete startup.", LogCategoriesEnum.INITIAL_STARTUP)
                logger.info("====================", LogCategoriesEnum.INITIAL_STARTUP)
                transaction?.finish()
                started = true

                gateway.awaitReady() // Block thread and wait for event to come
            } catch (e: Throwable) {
                logger.error("Start exception", LogCategoriesEnum.INITIAL_STARTUP, e)
            }
        }

        fun finishStartup(event: ReadyEvent) {
            getProcesses(EventListenerProcess::class.java).forEach {
                it.realReadyListeners?.invoke(event)
            }
            ConfigHandler.config.extraOnReadyListeners.forEach { it.invoke(gateway) }
            readyListeners.forEach { it.run() }
            running = true
        }

        private fun activateFeatures() {
            if (!ConfigHandler.config.supportChatCommands && (ConfigHandler.config.supportSlashCommands || ConfigHandler.config.supportLazyCommands)) {
                throw CantSupportAnyOtherCommandsTypeWithoutChatCommands()
            }
            if (ConfigHandler.config.createI18nOnBoot) {
                if (EnvConfig.SIMPLE_LOCALIZE_API_TOKEN == null) {
                    throw MissingSimpleLocalizedApiKey()
                }
            }
            if (ConfigHandler.config.supportChatCommands) {
                logger.info("Activated supportChatCommands")
                add(ChatCommandBot())
            }
            if (ConfigHandler.config.supportSlashCommands) {
                logger.info("Activated supportSlashCommands")
                add(SlashCommandBot())
            }
            if (ConfigHandler.config.liveConfigChannelId != null) {
                if (ConfigHandler.liveConfig == null) {
                    throw MissingDefaultLiveConfig()
                } else {
                    add(LiveConfigHandlerBot())
                }
            }
            if (ConfigHandler.config.supportLazyCommands) {
                if (ConfigHandler.config.lazyCommandsChannelId != null) {
                    logger.info("Activated supportLazyCommands")
                    add(LazyCommandsBot())
                } else {
                    throw MissingLazyCommandChannelId()
                }
            }
            if (ConfigHandler.config.supportDjDedicatedChannel || ConfigHandler.config.supportDjPlayerViaCommands) {
                logger.info("Activated DJ")
                add(DjQueueCommand()) // Required in any cases where the DJ is activated

                if (ConfigHandler.config.supportDjDedicatedChannel) {
                    if (ConfigHandler.config.djDedicatedChannelId != null) {
                        logger.info("Activated supportDjDedicatedChannel")
                        add(DjDedicatedChannelPlayerBot())
                    } else {
                        throw MissingDjChannelId()
                    }
                }

                if (ConfigHandler.config.supportDjPlayerViaCommands) {
                    logger.info("Added supportDjPlayerViaCommands")
                    add(DjToggleCommand())
                    add(DjNextCommand())
                    add(DjPreviousCommand())
                    add(DjLoopbackCommand())
                    add(DjConnectCommand())
                    add(DjClearQueueCommand())
                    add(DjStopCommand())
                    add(DjCurrentlyPlayingCommand())
                }
            }
            if (ConfigHandler.config.supportChatCommands) {
                if (ConfigHandler.config.addHelpCommand) {
                    logger.info("Added addHelpCommand")
                    add(HelpTextCommand())
                }
                if (ConfigHandler.config.addStatusCommand) {
                    logger.info("Added addHelpCommand")
                    add(StatusTextCommand())
                }
                if (ConfigHandler.config.addResetI18nCommand) {
                    logger.info("Added addHelpCommand")
                    add(ResetI18nCommand())
                }
            }
        }

        fun getProcesses() = getProcesses<Process>()
        fun <T : Process> getProcesses(vararg clazz: Class<out T>): MutableList<T> {
            return filterProcesses { process: Process ->
                clazz.isEmpty() || clazz.toList().stream().anyMatch { wanted ->
                    wanted.isAssignableFrom(process.javaClass)
                }
            }
        }

        fun <T : Process> getProcess(clazz: Class<out T>): T {
            val findAny = getProcesses(clazz).stream().findAny()
            return if (findAny.isPresent) {
                findAny.get()
            } else {
                throw ProcessNotFoundException(clazz)
            }
        }

        fun <T : Process> filterProcesses(filter: Function<Process, Boolean>): MutableList<T> {
            return processes.stream()
                .map {
                    @Suppress("UNCHECKED_CAST")
                    it as T
                }
                .filter { filter.apply(it) }
                .collect(Collectors.toList())
        }

        fun getCommands() = getCommands<Command>()
        fun <T : Command> getCommands(vararg clazz: Class<out T>): MutableList<T> {
            return filterCommands { command: Command ->
                clazz.isEmpty() || clazz.toList().stream().anyMatch { wanted ->
                    wanted.isAssignableFrom(command.javaClass)
                }
            }
        }

        fun <T : Command> getCommand(clazz: Class<out T>): T {
            val findAny = getCommands(clazz).stream().findAny()
            return if (findAny.isPresent) {
                findAny.get()
            } else {
                throw CommandNotFoundException(clazz)
            }
        }

        fun <T : Command> filterCommands(filter: Function<Command, Boolean>): MutableList<T> {
            return commands.stream()
                .map {
                    @Suppress("UNCHECKED_CAST")
                    it as T
                }
                .filter { filter.apply(it) }
                .collect(Collectors.toList())
        }
    }
}
