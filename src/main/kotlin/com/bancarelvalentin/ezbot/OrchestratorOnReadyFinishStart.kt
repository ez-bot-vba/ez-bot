package com.bancarelvalentin.ezbot

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.i18n.I18nAutoRegistrator
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.utils.SentryUtils
import com.bancarelvalentin.ezbot.utils.SlashCommandRegistrationUtils
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class OrchestratorOnReadyFinishStart : ListenerAdapter() {

    private val logger = Logger(OrchestratorOnReadyFinishStart::class.java)

    override fun onReady(event: ReadyEvent) {
        try {
            val transaction = SentryUtils.createPerf("[START] final startup", ConfigHandler.config.timeStartupInSentry, null)
            logger.info("====================", LogCategoriesEnum.STARTUP_POST_READY)
            logger.info("Post ready started.", LogCategoriesEnum.STARTUP_POST_READY)
            if (ConfigHandler.config.supportSlashCommands) {
                if (ConfigHandler.config.deleteExistingSlashCommandsOnBoot) {
                    logger.info("Doing deleteExistingSlashCommandsOnBoot")
                    deleteAllSlashCommands(event)
                }
                if (ConfigHandler.config.autoRegisterSlashCommands) {
                    logger.info("Doing autoRegisterSlashCommands")
                    registerAllSlashComnands(event)
                }
            }
            if (ConfigHandler.config.setPresenceOnBoot) {
                logger.info("Activated setPresenceOnBoot")
                event.jda.presence.setPresence(OnlineStatus.ONLINE, Activity.listening(ConfigHandler.config.presence))
            }
            if (ConfigHandler.config.createI18nOnBoot) {
                logger.info("Creating all i18n")
                Localizator.createMissingKeys = true
                I18nAutoRegistrator().registerAll()
            }
            logger.info("Post ready over. All services online !", LogCategoriesEnum.STARTUP_POST_READY)
            logger.info("====================", LogCategoriesEnum.STARTUP_POST_READY)
            transaction?.finish()
            Orchestrator.finishStartup(event)
        } catch (e: Throwable) {
            logger.error("Start exception", LogCategoriesEnum.STARTUP_POST_READY, e)
        }
    }

    private fun registerAllSlashComnands(event: ReadyEvent) {
        logger.info("Registering slash commands...", LogCategoriesEnum.STARTUP_POST_READY)
        Orchestrator.getCommands()
            .forEach { SlashCommandRegistrationUtils.register(event.jda, it) }
    }

    private fun deleteAllSlashCommands(event: ReadyEvent) {
        logger.info("Unegistering all existing slash commands...", LogCategoriesEnum.STARTUP_POST_READY)
        event.jda.retrieveCommands().queue { commands -> commands.forEach { command -> command.delete().queue() } }
    }
}
