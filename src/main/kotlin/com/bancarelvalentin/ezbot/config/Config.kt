package com.bancarelvalentin.ezbot.config

import ch.qos.logback.classic.Level
import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.default.commands.help.HelpTextCommand
import com.bancarelvalentin.ezbot.default.features.dj.DjEmoteAction
import net.dv8tion.jda.api.JDA
import java.awt.Color
import java.util.Locale

@Suppress("SameReturnValue")
interface Config {

    // Main
    val appLogLevel: Level
        get() = if (EnvConfig.DEV) Level.TRACE else Level.INFO

    val rootLogLevel: Level
        get() = if (EnvConfig.DEV) Level.DEBUG else Level.WARN

    val versionNumber: String?
        get() = null

    val version: String?
        get() = if (versionNumber != null) "v-$versionNumber" else null

    val prefixes: Array<String>
        get() = arrayOf("!")

    val extraOnReadyListeners: Array<(JDA) -> Unit>
        get() = emptyArray()

    val presence: String
        get() = if (!Orchestrator.started) "Starting..." else ConfigHandler.config.prefixes[0] + Orchestrator.getCommand(HelpTextCommand::class.java).getRawTriggers()[0]

    val language: Locale?
        get() = null

    val extraI18nProjectTokens: Array<String>
        get() = emptyArray()

    // Specific channels
    val errorLogsChannelId: Long?
        get() = null

    val lazyCommandsChannelId: Long?
        get() = null

    val liveConfigChannelId: Long?
        get() = null

    val djDedicatedChannelId: Long?
        get() = null

    val djDedicatedChannelExtraActionsEmotes: Array<DjEmoteAction>
        get() = emptyArray()

    val sentryDsn: String?
        get() = EnvConfig.SENTRY_DSN

    val simpleLocalizeApiKey: String?
        get() = EnvConfig.SIMPLE_LOCALIZE_API_TOKEN

    // Feature flags
    val supportChatCommands: Boolean
        get() = true

    val supportLazyCommands: Boolean
        get() = false

    val supportSlashCommands: Boolean
        get() = true

    val supportDjDedicatedChannel: Boolean
        get() = false

    val supportDjPlayerViaCommands: Boolean
        get() = false

    val addHelpCommand: Boolean
        get() = true

    val addStatusCommand: Boolean
        get() = true

    val addResetI18nCommand: Boolean
        get() = true

    val autoRegisterSlashCommands: Boolean
        get() = true

    val deleteExistingSlashCommandsOnBoot: Boolean
        get() = true

    val setPresenceOnBoot: Boolean
        get() = true

    val createI18nOnBoot: Boolean
        get() = false

    val logErrorsOnSentry: Boolean
        get() = false

    val timeStartupInSentry: Boolean
        get() = false

    val timeCommandExecutionsInSentry: Boolean
        get() = false

    val disableLiveConfigEdit: Boolean
        get() = false

    val timeCronExecutionsInSentry: Boolean

    // Access rights
    val moderatorsIds: Array<Long>
        get() = emptyArray()

    val moderatorsRolesIds: Array<Long>
        get() = emptyArray()

    val defaultWhitelistedGuildIds: Array<Long>
        get() = emptyArray()

    val defaultWhitelistedCategoriesIds: Array<Long>
        get() = emptyArray()

    val defaultWhitelistedChannelIds: Array<Long>
        get() = emptyArray()

    val defaultWhitelistedRoleIds: Array<Long>
        get() = emptyArray()

    val defaultWhitelistedUserIds: Array<Long>
        get() = emptyArray()

    val extraDjWhitelistedGuildIds: Array<Long>
        get() = emptyArray()

    val extraDjWhitelistedCategoriesIds: Array<Long>
        get() = emptyArray()

    val extraDjWhitelistedChannelIds: Array<Long>
        get() = emptyArray()

    val extraDjWhitelistedRoleIds: Array<Long>
        get() = emptyArray()

    val extraDjWhitelistedUserIds: Array<Long>
        get() = emptyArray()

    // Customizable values
    val defaultColor: Color
        get() = Color.WHITE
}
