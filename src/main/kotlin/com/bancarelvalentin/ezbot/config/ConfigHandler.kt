package com.bancarelvalentin.ezbot.config

import com.bancarelvalentin.ezbot.exception.implerrors.conf.ConfGetException
import com.bancarelvalentin.ezbot.exception.implerrors.conf.ConfSetException

class ConfigHandler {

    companion object {
        private var alreadyReturned = false
        private var customConf: Config? = null
        private var liveConf: LiveConfig? = null

        var config: Config
            get() {
                if (customConf == null) {
                    throw ConfGetException()
                }
                return customConf!!
            }
            set(value) {
                if (alreadyReturned) {
                    throw ConfSetException()
                }
                customConf = value
            }

        var liveConfig: LiveConfig?
            get() {
                return liveConf
            }
            set(value) {
                liveConf = value
            }
    }
}
