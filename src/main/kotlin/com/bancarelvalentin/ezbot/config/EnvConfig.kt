package com.bancarelvalentin.ezbot.config

import com.bancarelvalentin.ezbot.exception.implerrors.startup.NoApiTokenException
import java.nio.file.Path
import java.nio.file.Paths

@Suppress("unused")
class EnvConfig {
    companion object {
        val DEV = System.getenv("DEV")?.equals("true") ?: false
        val STORAGE_PATH = (if (DEV) Paths.get("/tmp/bot/data") else Paths.get("/data") as Path)!!
        val DISCORD_API_TOKEN: String = System.getenv("DISCORD_API_TOKEN") ?: throw NoApiTokenException()
        val SIMPLE_LOCALIZE_API_TOKEN: String? = System.getenv("SIMPLE_LOCALIZE_API_TOKEN")
        val SENTRY_DSN: String? = System.getenv("SENTRY_DSN")
    }
}
