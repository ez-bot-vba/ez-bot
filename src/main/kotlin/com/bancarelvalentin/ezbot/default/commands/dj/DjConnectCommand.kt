package com.bancarelvalentin.ezbot.default.commands.dj

import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.exception.usererrors.player.MustBeInAVoiceChannel
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import java.util.function.BiConsumer

open class DjConnectCommand : AbstractDjCommand("connect", "c") {
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_CONNECT_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_CONNECT_DESC)

    override val djLogic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        request.guild?.retrieveMember(request.user)?.queue {
            val channel = it?.voiceState?.channel ?: throw MustBeInAVoiceChannel()
            EzPlayer.connect(channel)
        }
    }
}
