package com.bancarelvalentin.ezbot.default.commands.dj

import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import java.util.function.BiConsumer

open class DjRepeatAllCommand : AbstractDjCommand("random", "ra") {
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_REPEAT_ALL_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_REPEAT_ALL_DESC)

    override val djLogic = BiConsumer { _: CommandRequest, _: CommandResponse ->
        EzPlayer.toggleRepeatAll()
    }
}
