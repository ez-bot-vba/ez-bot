package com.bancarelvalentin.ezbot.default.commands.dj

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam

class QueryParam : StringCommandParam() {
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_PARAM_DOC_DJ_QUERY_NAME)
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_PARAM_DOC_DJ_QUERY_DESC)
}
