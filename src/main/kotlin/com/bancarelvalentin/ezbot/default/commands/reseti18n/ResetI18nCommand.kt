package com.bancarelvalentin.ezbot.default.commands.reseti18n

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.ModeratorCommand
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import java.util.function.BiConsumer

class ResetI18nCommand : ModeratorCommand() {

    override val patterns = arrayOf("reset_i18n")
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_RESET_I18N_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_RESET_I18N_DESC)
    override val logic = BiConsumer { _: CommandRequest, _: CommandResponse -> Localizator.reset() }
}
