package com.bancarelvalentin.ezbot.default.commands.status

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import net.dv8tion.jda.api.EmbedBuilder
import java.util.function.BiConsumer

class StatusTextCommand : Command() {

    override val patterns = arrayOf("status", "s")
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_STATUS_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_STATUS_DESC)

    @Suppress("UNUSED_ANONYMOUS_PARAMETER")
    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        response.embedOverride = getEmbed()
    }

    private fun getEmbed(): EmbedBuilder {
        val embedBuilder = EmbedBuilder()
            .setColor(ConfigHandler.config.defaultColor)
            .setTitle(rawName)

        Orchestrator.getProcesses().forEach { b: Process ->
            embedBuilder.addField(b.generateDocName(), b.generateDocDesc(), false)
        }
        return embedBuilder
    }
}
