package com.bancarelvalentin.ezbot.default.features.chatcommands

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.exception.implerrors.commands.TooManyCommandMatches
import com.bancarelvalentin.ezbot.exception.implerrors.commands.UnknownChatCommandMessageType
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.handlers.ChatCommandHandler
import net.dv8tion.jda.api.events.message.GenericMessageEvent
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.events.message.MessageUpdateEvent

class ChatCommandListener(process: Process) : EventListener(process) {

    override fun onMessageUpdate(event: MessageUpdateEvent) {
        generic(event)
    }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        generic(event)
    }

    private fun generic(event: GenericMessageEvent) {
        val message = when (event) {
            is MessageUpdateEvent -> event.message
            is MessageReceivedEvent -> event.message
            else -> throw UnknownChatCommandMessageType(event::class.java)
        }

        val parseChatCommands = ChatCommandHandler.potentialCommand(message)
        val key = parseChatCommands?.second ?: return

        val matchingCommands = Orchestrator.filterCommands<Command> { command ->
            command.regexes.any { key.matches(it) }
        }
        if (matchingCommands.size > 1) {
            throw TooManyCommandMatches()
        } else if (matchingCommands.isNotEmpty()) {
            ChatCommandHandler(parseChatCommands, matchingCommands[0], event).handle()
        }
    }
}
