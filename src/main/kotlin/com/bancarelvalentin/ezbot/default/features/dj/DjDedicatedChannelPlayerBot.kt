package com.bancarelvalentin.ezbot.default.features.dj

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.ezbot.utils.EmojiEnum

class DjDedicatedChannelPlayerBot : BackgroundProcess() {

    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_EMOTE_PLAYER_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_EMOTE_PLAYER_DESC)

    val actionHandler = DjEmotePlayerEventListener(this)
    val discordLogger = DjLogger(this)
    val specificChannelQueuer = DjQueuerViaDedicatedChannel(this)

    var djEmoteActions = arrayOf(
        DjEmoteAction(EmojiEnum.RIGHT_ARROW_CURVING_LEFT, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_REPEAT_CURRENT)) { _, _, _ -> EzPlayer.loopback() },
        DjEmoteAction(EmojiEnum.LAST_TRACK_BUTTON, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_PREVIOUS)) { _, _, _ -> EzPlayer.previous() },
        DjEmoteAction(EmojiEnum.PLAY_OR_PAUSE_BUTTON, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_TOGGLE_PLAY_PAUSE)) { user, _, reactEvent ->
            reactEvent.guild.retrieveMember(user).queue { EzPlayer.playPause(it.voiceState?.channel) }
        },
        DjEmoteAction(EmojiEnum.NEXT_TRACK_BUTTON, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_NEXT)) { _, _, _ -> EzPlayer.next() },
        DjEmoteAction(EmojiEnum.RED_SQUARE, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_STOP)) { _, _, _ -> EzPlayer.stop() },
        DjEmoteAction(EmojiEnum.SHUFFLE_TRACKS_BUTTON, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_SHUFFLE)) { _, _, _ -> EzPlayer.shuffle() },
        DjEmoteAction(EmojiEnum.REPEAT_BUTTON, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_TOGGLE_REPEAT_ALL)) { _, _, _ -> EzPlayer.toggleRepeatAll() },
        DjEmoteAction(EmojiEnum.REPEAT_SINGLE_BUTTON, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_TOGGLE_REPEAT_ONE)) { _, _, _ -> EzPlayer.toggleRepeatOne() },
        DjEmoteAction(EmojiEnum.WASTEBASKET, Localizator.get(DefaultI18n.DJ_PLAYER_ACTION_RESET_QUEUE)) { _, _, _ -> EzPlayer.resetQueue() },
        *ConfigHandler.config.djDedicatedChannelExtraActionsEmotes
    )

    override fun getListeners(): Array<EventListener> {
        return arrayOf(actionHandler, discordLogger, specificChannelQueuer)
    }
}
