package com.bancarelvalentin.ezbot.default.features.dj

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.exception.usererrors.player.MustBeInAVoiceChannel
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.time.Duration

class DjQueuerViaDedicatedChannel(override val process: DjDedicatedChannelPlayerBot) : EventListener(process) {

    override fun onMessageReceived(event: MessageReceivedEvent) {
        super.onMessageReceived(event)
        if (ConfigHandler.config.djDedicatedChannelId!! != event.message.channel.idLong) return
        try {
            if (event.message.contentRaw.startsWith("http")) {
                val channel = event.member?.voiceState?.channel ?: throw MustBeInAVoiceChannel()
                EzPlayer.searchAndQueue(
                    event.message.contentRaw, channel,
                    { event.message.addReaction(EmojiEnum.CHECK_MARK.tounicode()).queue() },
                    { event.message.reply(if (it is AbstractUserException) it.message else Localizator.get(DefaultI18n.USER_ERROR_UNKNOWN)).queue() },
                )
            } else {
                // TODO
            }
        } catch (e: java.lang.Exception) {
            DjLogger.error(null, e)
        }
        if (!event.message.author.isBot) {
            AsyncUtils.delay(Duration.ofSeconds(30)) { event.message.delete().queue() }
        }
    }
}
