package com.bancarelvalentin.ezbot.default.features.lazycommands

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import net.dv8tion.jda.api.entities.TextChannel

class LazyCommandsBot : BackgroundProcess() {

    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_LAZYCOMMANDHANDLER_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_LAZYCOMMANDHANDLER_DESC)
    lateinit var channel: TextChannel
    val emote: String = EmojiEnum.FIRE.tounicode()

    override fun getListeners(): Array<EventListener> = arrayOf(LazyCommandsReadyListener(this), LazyCommandsReactionListener(this))
}
