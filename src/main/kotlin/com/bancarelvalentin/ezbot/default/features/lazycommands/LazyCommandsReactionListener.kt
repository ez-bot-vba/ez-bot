package com.bancarelvalentin.ezbot.default.features.lazycommands

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.exception.implerrors.commands.TooManyCommandMatches
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.handlers.LazyCommandHandler
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class LazyCommandsReactionListener(override val process: LazyCommandsBot) : EventListener(process) {

    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        super.onMessageReactionAdd(event)
        event.retrieveMessage().queue {
            if (event.user!!.isBot) return@queue
            if (event.channel.idLong != process.channel.idLong) return@queue
            if (it.embeds.isEmpty()) return@queue
            if (it.embeds[0].title == null) return@queue

            val matchingCommands = Orchestrator.filterCommands<Command> { command ->
                it.embeds[0].title!!.contains("`${command.patterns[0]}`")
            }

            if (matchingCommands.size > 1) {
                throw TooManyCommandMatches()
            } else if (matchingCommands.isNotEmpty()) {
                LazyCommandHandler(matchingCommands[0], event).handle()
            }

            event.retrieveMessage().complete().removeReaction(process.emote, event.user!!).complete()
        }
    }
}
