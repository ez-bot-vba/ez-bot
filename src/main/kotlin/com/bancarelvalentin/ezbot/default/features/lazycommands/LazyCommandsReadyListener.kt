package com.bancarelvalentin.ezbot.default.features.lazycommands

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.startup.LazyCommandsChannelNotFound
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.Constants
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.ReadyEvent

class LazyCommandsReadyListener(override val process: LazyCommandsBot) : EventListener(process) {

    private val introduction = EmbedBuilder()
        .setTitle(Localizator.get(DefaultI18n.LAZY_COMMANDS_INTRO_TITLE))
        .setDescription(Localizator.get(DefaultI18n.LAZY_COMMANDS_INTRO_TITLE_DESC))
        .setFooter(Localizator.get(DefaultI18n.LAZY_COMMANDS_INTRO_FOOTER))
        .build()

    override fun onReady(event: ReadyEvent) {
        process.channel = gateway.getTextChannelById(ConfigHandler.config.lazyCommandsChannelId!!) ?: throw LazyCommandsChannelNotFound()
        ChannelUtils.emptyChannel(process.channel)
        process.channel.sendMessageEmbeds(introduction).queue()
        Orchestrator.getCommands()
            .asSequence()
            .filter { it.whitelistRoleIds.isEmpty() || it.whitelistUserIds.isEmpty() } // We don't want to leak command that are not available publicly on the server
            .forEach { addLazyCommand(it) }
    }

    private fun addLazyCommand(it: Command) {
        val docEmbedBuilder = it.getDocEmbedBuilder()
        if (it.paramsClasses.isNotEmpty()) {
            if (it.paramsClasses.any { param -> !param.getDeclaredConstructor().newInstance().optional }) {
                docEmbedBuilder.setColor(Constants.COLOR_ERROR).setFooter(EmojiEnum.CONSTRUCTION.tounicode() + " you cannot pass param yet AND ONE IS REQUIRED. " + EmojiEnum.RED_CIRCLE.tounicode())
            } else {
                docEmbedBuilder.setColor(Constants.COLOR_WARNING).setFooter(EmojiEnum.CONSTRUCTION.tounicode() + " you cannot pass param yet.")
            }
        }
        process.channel.sendMessageEmbeds(docEmbedBuilder.build()).queue {
            it.addReaction(process.emote).queue()
        }
    }
}
