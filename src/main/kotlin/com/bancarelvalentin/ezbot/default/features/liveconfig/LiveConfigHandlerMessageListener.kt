package com.bancarelvalentin.ezbot.default.features.liveconfig

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.utils.Constants
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class LiveConfigHandlerMessageListener(override val process: LiveConfigHandlerBot) : EventListener(process) {
    override fun onMessageReceived(event: MessageReceivedEvent) {
        super.onMessageReceived(event)
        if (ConfigHandler.config.disableLiveConfigEdit) return
        if (event.channel.idLong != ConfigHandler.config.liveConfigChannelId!!) return
        if (event.message.author.isBot) {
            if (ConfigHandler.config.disableLiveConfigEdit) {
                // Config handled by another bot that just sent a new one; we have to reload
                LiveConfigHandlerReadyListener.reload()
            }
        } else {
            val json = event.message.contentRaw
            try {
                LiveConfigHandlerBot.load(json)
                event.message.delete().queue()
            } catch (e: java.lang.Exception) {
                event.message.replyEmbeds(EmbedBuilder().setTitle(e.javaClass.simpleName).setDescription(e.message).setColor(Constants.COLOR_ERROR).build()).queue()
            }
        }
    }
}
