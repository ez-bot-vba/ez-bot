package com.bancarelvalentin.ezbot.default.features.liveconfig

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.startup.LiveConfigChannelNotFound
import com.bancarelvalentin.ezbot.process.EventListener
import com.fasterxml.jackson.databind.ObjectMapper
import net.dv8tion.jda.api.events.ReadyEvent

class LiveConfigHandlerReadyListener(override val process: LiveConfigHandlerBot) : EventListener(process) {

    override fun onReady(event: ReadyEvent) {
        LiveConfigHandlerReadyListener.process = process
        LiveConfigHandlerBot.channel = gateway.getTextChannelById(ConfigHandler.config.liveConfigChannelId!!) ?: throw LiveConfigChannelNotFound()
        reload()
    }

    companion object {
        lateinit var process: LiveConfigHandlerBot

        fun reload() {
            val history = LiveConfigHandlerBot.channel.getHistoryFromBeginning(1).complete()
            val json: String = if (history.isEmpty) {
                ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ConfigHandler.liveConfig)
            } else {
                val existing = history.retrievedHistory[0].contentRaw.replace("\r\n", "\n")
                val split = existing.split("\n").toTypedArray()
                split.copyOfRange(1, split.size - 1).joinToString("\n")
            }
            LiveConfigHandlerBot.load(json)
        }
    }
}
