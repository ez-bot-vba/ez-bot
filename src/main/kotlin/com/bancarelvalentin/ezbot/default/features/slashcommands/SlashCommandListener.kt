package com.bancarelvalentin.ezbot.default.features.slashcommands

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.exception.implerrors.commands.TooManyCommandMatches
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.handlers.SlashCommandHandler
import com.bancarelvalentin.ezbot.utils.SlashCommandRegistrationUtils
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent

class SlashCommandListener(process: Process) : EventListener(process) {

    override fun onSlashCommand(event: SlashCommandEvent) {
        if (event.user.isBot) return

        val matchingCommands = Orchestrator.filterCommands<Command> { command ->
            val name = event.name
            val formatSlashCommandDocTitle = SlashCommandRegistrationUtils.formatSlashCommandDocTitle(command.patterns[0])
            name == formatSlashCommandDocTitle
        }

        if (matchingCommands.size > 1) {
            throw TooManyCommandMatches()
        } else if (matchingCommands.isNotEmpty()) {
            SlashCommandHandler(matchingCommands[0], event).handle()
        }
    }
}
