package com.bancarelvalentin.ezbot.dj

import com.bancarelvalentin.ezbot.default.features.dj.DjLogger
import com.bancarelvalentin.ezbot.exception.DiscordBotException
import com.bancarelvalentin.ezbot.exception.usererrors.player.MustBeInAVoiceChannel
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.event.AudioEvent
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventListener
import com.sedmelluq.discord.lavaplayer.player.event.PlayerPauseEvent
import com.sedmelluq.discord.lavaplayer.player.event.PlayerResumeEvent
import com.sedmelluq.discord.lavaplayer.player.event.TrackEndEvent
import com.sedmelluq.discord.lavaplayer.player.event.TrackExceptionEvent
import com.sedmelluq.discord.lavaplayer.player.event.TrackStartEvent
import com.sedmelluq.discord.lavaplayer.player.event.TrackStuckEvent
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame
import net.dv8tion.jda.api.audio.AudioSendHandler
import net.dv8tion.jda.api.entities.AudioChannel
import net.dv8tion.jda.api.managers.AudioManager
import java.nio.ByteBuffer

class EzPlayer : AudioEventListener, AudioSendHandler {

    // TODO
    // SoundCloud
    // Twitch streams
    // Local files
    // HTTP URLs

    companion object {
        val state: State
            get() {
                return instance.state
            }
        val instance = EzPlayer()

        @Suppress("unused")
        fun searchAndQueueFirst(paramString: String, channel: AudioChannel? = null, onSuccess: ((Array<AudioTrackInfo>) -> Unit)? = null, onError: ((DiscordBotException) -> Unit)? = null) =
            instance.searchAndQueueFirst(paramString, channel, onSuccess, onError)

        fun searchAndQueue(paramString: String, channel: AudioChannel? = null, onSuccess: ((Array<AudioTrackInfo>) -> Unit)? = null, onError: ((DiscordBotException) -> Unit)? = null) = instance.searchAndQueue(paramString, channel, onSuccess, onError)
        fun next() = instance.next(NextReason.USER_REQQUEST)
        fun previous() = instance.previous()
        fun playPause(channel: AudioChannel?) = instance.playPause(channel)
        fun connect(channel: AudioChannel?) = instance.connectIfneeded(channel)
        fun playingTrack(): AudioTrack? = instance.playingTrack
        fun stop() = instance.stop()
        fun resetQueue() = instance.resetQueue()
        fun loopback() = instance.loopback()
        fun shuffle() = instance.shuffle()
        fun toggleRepeatAll() = instance.toggleRepeatAll()
        fun toggleRepeatOne() = instance.toggleRepeatOne()

        fun isRepeatingAll() = instance.repeatAll
        fun isRepeatingOne() = instance.repeatOne

        fun getQueue(count: Int): List<AudioTrackInfo> {
            val realCount = minOf(count, instance.queue.size)
            return instance.queue.subList(0, realCount).map { it.info }
        }

        fun getHistory(count: Int): List<AudioTrackInfo> {
            val size = instance.history.size
            val realCount = minOf(count, size)

            val start = size - realCount
            return instance.history.subList(start, size).map { it.info }
        }
    }

    private val logger = Logger(EzPlayer::class.java)

    private val playerManager: AudioPlayerManager = DefaultAudioPlayerManager()
    private val player = playerManager.createPlayer()
    private var audioManager: AudioManager? = null

    private var history = ArrayList<AudioTrack>()
    private var queue = ArrayList<AudioTrack>()
    private var playingTrack: AudioTrack? = null
    private var state = State.STOPPED
    private var lastFrame: AudioFrame? = null
    private var repeatAll: Boolean = false
    private var repeatOne: Boolean = false

    init {
        logger.warn("EzPlayer instantiated. Should be only once.")
        AudioSourceManagers.registerRemoteSources(playerManager)
        player.addListener(this)
    }

    override fun isOpus() = true
    override fun provide20MsAudio(): ByteBuffer = ByteBuffer.wrap(lastFrame!!.data)

    override fun canProvide(): Boolean {
        if (state != State.PLAYING) return false

        lastFrame = player.provide()
        val canProvide = lastFrame != null
        return canProvide
    }

    override fun onEvent(event: AudioEvent?) {
        logger.debug("audio event: ${event?.javaClass?.simpleName ?: "no event"}", LogCategoriesEnum.DJ)
        when (event) {
            is PlayerResumeEvent -> {
                DjLogger.resumed(playingTrack!!.info)
            }
            is PlayerPauseEvent -> {
                DjLogger.paused()
            }
            is TrackStartEvent -> {
                DjLogger.nowPlaying(event.track.info)
            }
            is TrackEndEvent -> {
                when (event.endReason) {
                    AudioTrackEndReason.FINISHED, AudioTrackEndReason.LOAD_FAILED -> {
                        logger.info("Automatically starting next track")
                        next(NextReason.TRACK_ENDED)
                    }
                    AudioTrackEndReason.CLEANUP -> {
                        logger.info("Paused for too long; stopping")
                        disconnect()
                        DjLogger.stoppedForinactivity()
                    }
                    AudioTrackEndReason.STOPPED -> {
                        disconnect()
                    }
                    else -> {}
                }
            }
            is TrackStuckEvent -> {
                logger.warn("TrackStuckEvent: ")
                DjLogger.stuck(event.track.info)
            }
            is TrackExceptionEvent -> {
                logger.warn("TrackExceptionEvent: ", throwable = event.exception)
                DjLogger.error(event.track.info, event.exception)
            }
        }
    }

    fun searchAndQueueFirst(paramString: String, channel: AudioChannel? = null, onSuccess: ((Array<AudioTrackInfo>) -> Unit)? = null, onError: ((DiscordBotException) -> Unit)? = null) {
        search(paramString, channel, true, onSuccess, onError)
    }

    fun searchAndQueue(paramString: String, channel: AudioChannel? = null, onSuccess: ((Array<AudioTrackInfo>) -> Unit)? = null, onError: ((DiscordBotException) -> Unit)? = null) {
        search(paramString, channel, false, onSuccess, onError)
    }

    private fun search(paramString: String, channel: AudioChannel? = null, addFirst: Boolean = false, onSuccess: ((Array<AudioTrackInfo>) -> Unit)? = null, onError: ((DiscordBotException) -> Unit)? = null) {
        playerManager.loadItem(paramString, SearchResultHandler(channel, addFirst, onSuccess, onError, onLoad))
    }

    private val onLoad: (Boolean, AudioChannel?, Array<AudioTrack>) -> Unit = { addFirst, channel, tracks ->
        if (addFirst) queue = ArrayList(arrayOf(*tracks, *queue.toTypedArray()).toList())
        else queue.addAll(tracks)
        DjLogger.queued(tracks)

        logQeueu()
        logger.info("Queued ${tracks.size} tracks")

        connectIfneeded(channel)
        if (state != State.PLAYING) {
            logger.info("Starting because it's first track")
            next(NextReason.STARTING)
        }
    }

    private fun playPause(channel: AudioChannel?) {
        when (state) {
            State.STOPPED -> {
                logger.info("Starting")
                connectIfneeded(channel)
                state = State.PLAYING
                next(NextReason.STARTING)
            }
            State.PLAYING -> {
                logger.info("Pausing")
                state = State.PAUSED
            }
            State.PAUSED -> {
                logger.info("Resuming")
                connectIfneeded(channel)
                state = State.PLAYING
            }
        }
        logQeueu()
    }

    private fun logQeueu() {
        logger.debug("${state.name} | ${queue.size} remaining in queue")
    }

    private fun disconnect() {
        logger.info("Disconnecting voice")
        audioManager?.closeAudioConnection()
    }

    private fun connectIfneeded(channel: AudioChannel?) {
        logger.info("Connecting to voice")
        if (audioManager == null) {
            channel ?: throw MustBeInAVoiceChannel()
            audioManager = channel.guild.audioManager
        }

        audioManager!!.sendingHandler = instance
        if (!audioManager!!.isConnected) {
            audioManager!!.openAudioConnection(channel)
        }
    }

    private fun loopback() {
        if (playingTrack == null) return
        state = State.PLAYING
        player.playTrack(playingTrack!!.makeClone())
        DjLogger.refreshMeta()
    }

    private fun shuffle() {
        queue.shuffle()
        DjLogger.refreshMeta()
    }

    private fun toggleRepeatAll() {
        repeatAll = !repeatAll
        DjLogger.refreshMeta()
    }

    private fun toggleRepeatOne() {
        repeatOne = !repeatOne
        DjLogger.refreshMeta()
    }

    private fun previous() {
        if (history.isNotEmpty()) {
            logger.info("Playing previous song")

            if (playingTrack != null) {
                queue = ArrayList(arrayOf(playingTrack!!.makeClone(), *queue.toTypedArray()).toList())
            }

            val previous = history.removeLast()
            state = State.PLAYING
            playingTrack = previous.makeClone()
            player.playTrack(playingTrack)
            logQeueu()
        }
    }

    private fun next(reason: NextReason) {
        if ((!repeatOne || reason == NextReason.USER_REQQUEST) && queue.isEmpty()) {
            if (repeatAll) {
                history.forEach { queue.add(it.makeClone()) }
                if (playingTrack != null) {
                    queue.add(0, playingTrack!!.makeClone())
                }
                queue.reverse()
                history = ArrayList()
                playingTrack = null
                next(reason)
            } else {
                logger.info("No more songs")
                stop()
            }
            return
        }

        playingTrack = when (reason) {
            NextReason.STARTING -> {
                queue.removeFirst()
            }
            NextReason.USER_REQQUEST -> {
                if (playingTrack != null) {
                    history.add(playingTrack!!)
                }
                queue.removeFirst()
            }
            NextReason.TRACK_ENDED -> {
                if (playingTrack != null) {
                    if (repeatOne) {
                        playingTrack!!.makeClone()
                    } else {
                        history.add(playingTrack!!)
                        queue.removeFirst()
                    }
                } else {
                    queue.removeFirst()
                }
            }
        }
        state = State.PLAYING
        player.playTrack(playingTrack)
        logQeueu()
    }

    private fun resetQueue() {
        logQeueu()
        logger.info("Resetting queue")
        queue = ArrayList()
        history = ArrayList()
        logQeueu()
        DjLogger.emptyQueue()
    }

    private fun stop() {
        logger.info("Stopping player")
        state = State.STOPPED
        playingTrack = null
        player.stopTrack()
        disconnect()
        logQeueu()
        DjLogger.stopped()
    }
}
