package com.bancarelvalentin.ezbot.dj

enum class NextReason {
    STARTING,
    USER_REQQUEST,
    TRACK_ENDED
}
