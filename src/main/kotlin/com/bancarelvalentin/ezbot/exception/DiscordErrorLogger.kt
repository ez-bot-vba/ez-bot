package com.bancarelvalentin.ezbot.exception

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.exception.implerrors.UnknownErrorWrapper
import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.utils.ExceptionUtils
import net.dv8tion.jda.api.entities.MessageChannel
import java.awt.Color

class DiscordErrorLogger(val exception: Exception, val color: Color, val shorten: Boolean) {

    companion object {

        init {
            Orchestrator.listenForStart {
                missedExceptions.forEach { handle(it) }
            }
        }

        val missedExceptions = ArrayList<DiscordBotException>()
        val channel: MessageChannel? = if (ConfigHandler.config.errorLogsChannelId != null) Orchestrator.gateway.getTextChannelById(ConfigHandler.config.errorLogsChannelId!!) else null

        @Deprecated("Please use your own implementation of AbstractUserException and AbstractImplementationException", ReplaceWith("handle(exception: DiscordBotException)"))
        fun handleUnexpected(exception: Throwable) {
            handle(UnknownErrorWrapper(exception) as DiscordBotException)
        }

        fun handle(exception: DiscordBotException) {
            if (!Orchestrator.started) {
                Logger(DiscordErrorLogger::class.java).warn("Too soon to send error in discord; will do on bot ready", LogCategoriesEnum.INITIAL_STARTUP)
                missedExceptions.add(exception)
            }
            if (channel != null) {
                when (exception) {
                    is AbstractUserException -> DiscordErrorLogger(exception, Color.ORANGE, true).log()
                    is AbstractImplementationException -> DiscordErrorLogger(exception, Color.RED, false).log()
                    else -> handle(UnknownErrorWrapper(exception as Exception) as DiscordBotException)
                }
            }
        }
    }

    private fun log() {
        channel!!.sendMessageEmbeds(ExceptionUtils.toEmbed(exception, shorten, color)).queue()
    }
}
