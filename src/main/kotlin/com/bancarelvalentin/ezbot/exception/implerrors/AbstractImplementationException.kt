package com.bancarelvalentin.ezbot.exception.implerrors

import com.bancarelvalentin.ezbot.exception.DiscordBotException

abstract class AbstractImplementationException(message: String, cause: Throwable? = null) : DiscordBotException, RuntimeException(message, cause)
