package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.process.command.param.CommandParam

class CannotInstantiateParamException(paramClass: Class<out CommandParam<out Any?>>, override val cause: Exception) : AbstractImplementationException("Cannot instantiate param ${paramClass.simpleName}", cause)
