package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MultiplePotentialParamMatchesInASlashcommandException : AbstractImplementationException("A param given in a slash command could be mapped to 2 different classes.")
