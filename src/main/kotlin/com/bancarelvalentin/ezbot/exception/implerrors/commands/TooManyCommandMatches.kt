package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import net.dv8tion.jda.api.events.message.GenericMessageEvent

class TooManyCommandMatches(vararg clazz: Class<out GenericMessageEvent>) : AbstractImplementationException("More than one command match the given input (${clazz.joinToString { it.simpleName }})")
