package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import net.dv8tion.jda.api.entities.ChannelType

class UnknownSlashCommandChannelTypeException(type: ChannelType) : AbstractImplementationException("Channel type ${type.name} not supported in slash commands")
