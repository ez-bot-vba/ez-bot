package com.bancarelvalentin.ezbot.exception.implerrors.conf

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class ConfGetException : AbstractImplementationException("Conf accessed before it got initialized.")
