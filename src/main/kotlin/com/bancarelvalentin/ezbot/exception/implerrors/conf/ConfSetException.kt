package com.bancarelvalentin.ezbot.exception.implerrors.conf

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class ConfSetException : AbstractImplementationException("Conf overridden after it got accessed.")
