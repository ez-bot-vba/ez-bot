package com.bancarelvalentin.ezbot.exception.implerrors.conf

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MissingDefaultLiveConfig : AbstractImplementationException("You need to provide a default live config")
