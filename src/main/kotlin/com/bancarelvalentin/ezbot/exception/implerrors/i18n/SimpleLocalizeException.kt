package com.bancarelvalentin.ezbot.exception.implerrors.i18n

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class SimpleLocalizeException(reason: String) : AbstractImplementationException(reason)
