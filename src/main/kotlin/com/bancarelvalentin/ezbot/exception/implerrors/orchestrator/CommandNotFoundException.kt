package com.bancarelvalentin.ezbot.exception.implerrors.orchestrator

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.process.command.Command

class CommandNotFoundException(clazz: Class<out Command>) : AbstractImplementationException("Command $clazz not found in current running processes")
