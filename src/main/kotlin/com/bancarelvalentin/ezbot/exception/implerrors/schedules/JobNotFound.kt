package com.bancarelvalentin.ezbot.exception.implerrors.schedules

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class JobNotFound : AbstractImplementationException("Quartz job not found when trying to stop")
