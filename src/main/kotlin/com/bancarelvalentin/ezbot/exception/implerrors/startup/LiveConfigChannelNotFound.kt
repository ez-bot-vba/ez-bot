package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class LiveConfigChannelNotFound : AbstractImplementationException("Channel for live config not found")
