package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MissingDjChannelId : AbstractImplementationException("You need to provide a channel id in which to send player actions for the DJ")
