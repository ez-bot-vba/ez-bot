package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MissingLazyCommandChannelId : AbstractImplementationException("You need to provide a channel id in which to send lazy commands")
