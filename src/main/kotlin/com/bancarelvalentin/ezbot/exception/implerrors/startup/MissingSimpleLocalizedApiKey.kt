package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MissingSimpleLocalizedApiKey : AbstractImplementationException("You need to provide an API key to create SimpleLocalize.io keys")
