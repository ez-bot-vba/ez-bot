package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class NoApiTokenException : AbstractImplementationException("No discord API token provided")
