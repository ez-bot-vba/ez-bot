package com.bancarelvalentin.ezbot.exception.usererrors

import com.bancarelvalentin.ezbot.exception.DiscordBotException

abstract class AbstractUserException(override val message: String, cause: Exception? = null) : DiscordBotException, Exception(message, cause)
