package com.bancarelvalentin.ezbot.exception.usererrors

class GrantException(reason: String) : AbstractUserException(reason)
