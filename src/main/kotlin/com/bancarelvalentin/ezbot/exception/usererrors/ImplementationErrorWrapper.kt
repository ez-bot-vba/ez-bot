package com.bancarelvalentin.ezbot.exception.usererrors

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator

class ImplementationErrorWrapper(cause: AbstractImplementationException) : AbstractUserException(cause.message ?: Localizator.get(DefaultI18n.USER_ERROR_UNKNOWN), cause)
