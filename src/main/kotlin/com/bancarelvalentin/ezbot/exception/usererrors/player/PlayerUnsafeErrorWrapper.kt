package com.bancarelvalentin.ezbot.exception.usererrors.player

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException

class PlayerUnsafeErrorWrapper(exception: FriendlyException) : AbstractUserException(Localizator.get(DefaultI18n.USER_ERROR_PLAYER_ERROR_UNSAFE, exception.message))
