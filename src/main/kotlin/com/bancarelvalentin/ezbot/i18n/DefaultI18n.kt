package com.bancarelvalentin.ezbot.i18n

/**
 * !!! if you edit this file you must also edit the key name on its source (likely simpllocalize.io)
 */
enum class DefaultI18n : I18nDictionary {
    COMMAND_UNAUTHORIZED_GUILD,
    COMMAND_UNAUTHORIZED_CHANNEL_CATEGORY,
    COMMAND_UNAUTHORIZED_CHANNEL,
    COMMAND_UNAUTHORIZED_USER_OR_ROLE,
    COMMAND_UNAUTHORIZED_FAILED_TO_LOAD_USER,
    DEFAULT_PROCESS_DOC_HELP_NAME,
    DEFAULT_PROCESS_DOC_HELP_DESC,
    DEFAULT_PROCESS_DOC_STATUS_NAME,
    DEFAULT_PROCESS_DOC_STATUS_DESC,
    DEFAULT_PROCESS_DOC_RESET_I18N_NAME,
    DEFAULT_PROCESS_DOC_RESET_I18N_DESC,
    DEFAULT_PROCESS_PARAM_DOC_HELP_TARGET_NAME,
    DEFAULT_PROCESS_PARAM_DOC_HELP_TARGET_DESC,
    DEFAULT_PROCESS_DOC_DJ_EMOTE_PLAYER_NAME,
    DEFAULT_PROCESS_DOC_DJ_EMOTE_PLAYER_DESC,
    DEFAULT_PROCESS_DOC_DJ_CLEAR_QUEUE_NAME,
    DEFAULT_PROCESS_DOC_DJ_CLEAR_QUEUE_DESC,
    DEFAULT_PROCESS_DOC_DJ_LOOPBACK_NAME,
    DEFAULT_PROCESS_DOC_DJ_LOOPBACK_DESC,
    DEFAULT_PROCESS_DOC_DJ_PLAY_NEXT_NAME,
    DEFAULT_PROCESS_DOC_DJ_PLAY_NEXT_DESC,
    DEFAULT_PROCESS_DOC_DJ_PLAY_PREVIOUS_NAME,
    DEFAULT_PROCESS_DOC_DJ_PLAY_PREVIOUS_DESC,
    DEFAULT_PROCESS_DOC_DJ_ADD_TO_QUEUE_NAME,
    DEFAULT_PROCESS_DOC_DJ_ADD_TO_QUEUE_DESC,
    DEFAULT_PROCESS_DOC_DJ_CONNECT_NAME,
    DEFAULT_PROCESS_DOC_DJ_CONNECT_DESC,
    DEFAULT_PROCESS_DOC_DJ_STOP_NAME,
    DEFAULT_PROCESS_DOC_DJ_STOP_DESC,
    DEFAULT_PROCESS_DOC_DJ_TOGGLE_PLAY_PAUSE_NAME,
    DEFAULT_PROCESS_DOC_DJ_TOGGLE_PLAY_PAUSE_DESC,
    DEFAULT_PROCESS_DOC_DJ_CURRENTLY_PLAYING_NAME,
    DEFAULT_PROCESS_DOC_DJ_CURRENTLY_PLAYING_DESC,
    DEFAULT_PROCESS_DOC_DJ_SHUFFLE_NAME,
    DEFAULT_PROCESS_DOC_DJ_SHUFFLE_DESC,
    DEFAULT_PROCESS_DOC_DJ_REPEAT_ALL_NAME,
    DEFAULT_PROCESS_DOC_DJ_REPEAT_ALL_DESC,
    DEFAULT_PROCESS_DOC_DJ_REPEAT_ONE_NAME,
    DEFAULT_PROCESS_DOC_DJ_REPEAT_ONE_DESC,
    DEFAULT_PROCESS_PARAM_DOC_DJ_QUERY_NAME,
    DEFAULT_PROCESS_PARAM_DOC_DJ_QUERY_DESC,
    DEFAULT_PROCESS_DOC_LIVECONFIGHANDLER_NAME,
    DEFAULT_PROCESS_DOC_LIVECONFIGHANDLER_DESC,
    DJ_PLAYER_STATE_NO_TRACK_PLAYING,
    DJ_PLAYER_QUEUE_LABEL,
    DJ_PLAYER_HISTORY_LABEL,
    DJ_PLAYER_PLAYING_LABEL,
    DJ_PLAYER_ACTIONS_LABEL,
    DJ_PLAYER_REPEAT_ONE_LABEL,
    DJ_PLAYER_REPEAT_ALL_LABEL,
    DJ_PLAYER_ACTION_REPEAT_CURRENT,
    DJ_PLAYER_ACTION_TOGGLE_REPEAT_ONE,
    DJ_PLAYER_ACTION_TOGGLE_REPEAT_ALL,
    DJ_PLAYER_ACTION_SHUFFLE,
    DJ_PLAYER_ACTION_PREVIOUS,
    DJ_PLAYER_ACTION_TOGGLE_PLAY_PAUSE,
    DJ_PLAYER_ACTION_NEXT,
    DJ_PLAYER_ACTION_STOP,
    DJ_PLAYER_ACTION_RESET_QUEUE,
    DJ_PLAYER_LOG_NOW_PLAYING,
    DJ_PLAYER_LOG_PAUSED,
    DJ_PLAYER_LOG_STOPPED,
    DJ_PLAYER_LOG_STOPPED_FOR_INACTIVITY,
    DJ_PLAYER_LOG_EMPTYED_QUEUE,
    DJ_PLAYER_LOG_RESUMED,
    DJ_PLAYER_LOG_QUEUED,
    DJ_PLAYER_LOG_UNKNOWN_ERROR,
    DJ_PLAYER_LOG_UNKNOWN_ERROR_ON_TRACK,
    DJ_PLAYER_LOG_NOW_PLAYING_AUTHOR_LABEL,
    DJ_PLAYER_LOG_NOW_PLAYING_DURATION_LABEL,
    DJ_PLAYER_CHANNEL_TOPIC,
    DEFAULT_PROCESS_DOC_SLASHCOMMANDHANDLER_DESC,
    DEFAULT_PROCESS_DOC_SLASHCOMMANDHANDLER_NAME,
    DEFAULT_PROCESS_DOC_LAZYCOMMANDHANDLER_DESC,
    DEFAULT_PROCESS_DOC_LAZYCOMMANDHANDLER_NAME,
    DEFAULT_PROCESS_DOC_CHATCOMMANDHANDLER_DESC,
    DEFAULT_PROCESS_DOC_CHATCOMMANDHANDLER_NAME,
    HELP_COMMAND_CONTENT,
    LAZY_COMMANDS_INTRO_TITLE,
    LAZY_COMMANDS_INTRO_TITLE_DESC,
    LAZY_COMMANDS_INTRO_FOOTER,
    USER_ERROR_UNKNOWN,
    USER_ERROR_MISSING_PARAM,
    USER_ERROR_WRONG_PARAM_TYPE,
    USER_ERROR_MUST_BE_IN_A_VOICE_CHANNEL,
    USER_ERROR_NO_PLAYER_RESULTS,
    USER_ERROR_PLAYER_ERROR_SAFE,
    USER_ERROR_PLAYER_ERROR_UNSAFE,
    COMMAND_DOC_SYNTAXE,
    COMMAND_DOC_EXAMPLE,
    COMMAND_DOC_CHANNELS,
    COMMAND_DOC_GRANTS,
    COMMAND_DOC_EXTRA_PARAM_NAME,
    COMMAND_DOC_EXTRA_PARAM_DESC,
    COMMAND_DOC_PARAM_TYPE_USER,
    COMMAND_DOC_PARAM_TYPE_CHANNEL,
    COMMAND_DOC_PARAM_TYPE_ROLE,
    COMMAND_DOC_PARAM_TYPE_INTEGER,
    COMMAND_DOC_PARAM_TYPE_BOOLEAN,
    COMMAND_DOC_PARAM_TYPE_NUMBER_OR_MATH,
    COMMAND_DOC_PARAM_TYPE_STRING,
    ;

    override val key: String = I18nDictionary.enumToKey(this)
}
