package com.bancarelvalentin.ezbot.i18n

import com.bancarelvalentin.ezbot.utils.ReflectionUtils
import javassist.Modifier
import java.util.stream.Stream

class I18nAutoRegistrator {

    fun registerAll() {
        val classes: Stream<Class<out I18nDictionary>> = ReflectionUtils.getAllClassesOfType()
        classes.filter { Modifier.isEnum(it.modifiers) }
            .flatMap { it.enumConstants.toList().stream() }
            .forEach { dicValue -> Localizator.get(dicValue) }
    }
}
