package com.bancarelvalentin.ezbot.i18n

interface I18nDictionary {

    val key: String

    companion object {
        fun <E : Enum<E>> enumToKey(enum: E): String {
            return enum.name
                .toLowerCase()
                .replace("_", ".")
                .replace(Regex("\\.+"), ".")
        }
    }
}
