package com.bancarelvalentin.ezbot.i18n

class I18nIndex(val lang: Language? = null) : HashMap<String, String>() {

    fun add(pI18nIndex: I18nIndex) {
        pI18nIndex.keys.forEach {
            if (this[it] == null) {
                this[it] = pI18nIndex[it]!!
            }
        }
    }
}
