package com.bancarelvalentin.ezbot.i18n

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.startup.DefaultLanguageNotFound
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.utils.Constants
import java.text.MessageFormat
import java.util.Locale

class Localizator {

    companion object {

        private val logger = Logger(Localizator::class.java)
        private val allOrderedTokens = arrayOf(*ConfigHandler.config.extraI18nProjectTokens, Constants.I18N_PROJECT_TOKEN)
        private var language = retrieveDefaultLanguage()
        private var indexes = HashMap<String, I18nIndex>()
        private var initialized = false
        var createMissingKeys = false

        fun reset() {
            logger.info("Resetting all indexes")
            init(true)
        }

        private fun init(forced: Boolean = false) {
            if (initialized && !forced) return
            if (forced) indexes = HashMap()
            logger.info("Language set to '$language'")
            buildIndexes()
            logger.info("i18n strings imported")
            initialized = true
        }

        private fun buildIndexes() {
            for (token in allOrderedTokens) {
                val indexMap = SimpleLocalizeIoCaller.getIndexes(token)
                load(*indexMap.values.toTypedArray())
            }
        }

        @Suppress("MemberVisibilityCanBePrivate")
        fun load(vararg pI18nIndices: I18nIndex) {
            for (index in pI18nIndices) {
                val langStr = index.lang!!.key
                if (indexes[langStr] == null) {
                    indexes[langStr] = I18nIndex(Language(langStr))
                }
                indexes[langStr]!!.add(index)
            }
        }

        private fun retrieveDefaultLanguage(): String {
            val hardCodedDefault = localeToLangKey(ConfigHandler.config.language)
            if (hardCodedDefault != null) {
                return hardCodedDefault
            } else {
                for (token in allOrderedTokens)
                    for (language in SimpleLocalizeIoCaller.getLanguages(token))
                        if (language.isDefault == true)
                            return language.key
            }
            throw DefaultLanguageNotFound()
        }

        fun get(localizeEnum: I18nDictionary, locale: Locale?, vararg args: Any?): String {
            @Suppress("DEPRECATION")
            return get(localizeEnum.key, locale, *args)
        }

        fun get(localizeEnum: I18nDictionary, vararg args: Any?): String {
            @Suppress("DEPRECATION")
            return get(localizeEnum.key, null, *args)
        }

        @Deprecated("Please create an enunm extended SimpleLocalizeDictionary")
        fun get(key: String, locale: Locale?, vararg args: Any?): String {
            val safeArgs = args.map { it ?: "null" }.toTypedArray()
            init()
            val lang = localeToLangKey(locale) ?: language
            val value = indexes[lang]?.get(key)
            if (value == null) {
                logger.warn("Property not found: $lang/$key (${safeArgs.joinToString { it.toString() }})")
                if (createMissingKeys) {
                    SimpleLocalizeIoCaller.addKey(key, language)
                }
                return "??? $key ???"
            }
            if (value.isBlank()) {
                logger.debug("Property empty : $lang/$key (${safeArgs.joinToString { it.toString() }})")
                return "!!! $key !!!"
            }

            return MessageFormat.format(value, *safeArgs).replace("\\n", "\n").replace("\\t", "\t")
        }

        private fun localeToLangKey(l: Locale?) = l?.toLanguageTag()?.replace(Regex("[\\W\\-]"), "_")
    }
}
