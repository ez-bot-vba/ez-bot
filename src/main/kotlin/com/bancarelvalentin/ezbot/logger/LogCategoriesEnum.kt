package com.bancarelvalentin.ezbot.logger

@Suppress("unused")
enum class LogCategoriesEnum : LogCategory {
    DJ,
    CRON,
    CRON_JOB,
    ANY_COMMAND,
    CHAT_COMMAND,
    SLASH_COMMAND,
    LAZY_COMMAND,
    GENERIC_COMMAND,
    DEFAULT,
    INITIAL_STARTUP,
    STARTUP_POST_READY;

    override val code = this.name
}
