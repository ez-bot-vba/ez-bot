package com.bancarelvalentin.ezbot.logger

interface LogCategory {
    val code: String
}
