package com.bancarelvalentin.ezbot.process

import com.bancarelvalentin.ezbot.logger.Logger
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.hooks.ListenerAdapter

@Suppress("unused")
abstract class EventListener(open val process: Process) : ListenerAdapter() {

    protected val logger: Logger
        get() {
            return process.logger
        }

    protected val gateway: JDA
        get() {
            return process.gateway
        }
}
