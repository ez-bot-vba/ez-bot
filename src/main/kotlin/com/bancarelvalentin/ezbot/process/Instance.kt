package com.bancarelvalentin.ezbot.process

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.process.addreaction.AddReactionNewReactionListener
import com.bancarelvalentin.ezbot.process.addreaction.AddReactionParams
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Emote
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent
import java.util.UUID
import java.util.function.Consumer

interface Instance {

    val gateway: JDA
        get() = Orchestrator.gateway
    val uuid: UUID
        get() = UUID.randomUUID()
    val logger: Logger
        get() = Logger(this.javaClass)

    val onReaction: Array<Consumer<MessageReactionAddEvent>>
        get() = emptyArray()

    fun addReactionEvent(message: Message, reaction: Emote, params: AddReactionParams?, logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit) {
        return addReactionEventGeneric(message, params, logic, reactionEmote = reaction)
    }

    fun addReactionEvent(message: Message, reaction: String, params: AddReactionParams?, logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit) {
        return addReactionEventGeneric(message, params, logic, reactionString = reaction)
    }

    fun addReactionEvent(message: Message, reaction: EmojiEnum, params: AddReactionParams?, logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit) {
        return addReactionEventGeneric(message, params, logic, reactionString = reaction.tounicode())
    }

    fun addReactionEvent(message: Message, params: AddReactionParams?, logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit) {
        return addReactionEventGeneric(message, params, logic)
    }

    private fun addReactionEventGeneric(
        message: Message,
        params: AddReactionParams?,
        logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit,
        reactionEmote: Emote? = null,
        reactionString: String? = null,
    ) {
        gateway.addEventListener(AddReactionNewReactionListener(this, message, params, logic, reactionEmote, reactionString))
    }
}
