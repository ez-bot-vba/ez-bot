package com.bancarelvalentin.ezbot.process.addreaction

import com.bancarelvalentin.ezbot.process.Instance
import net.dv8tion.jda.api.entities.Emote
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class AddReactionNewReactionListener(
    private val instance: Instance,
    private val message: Message,
    private val params: AddReactionParams?,
    private val logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit,
    private val reactionEmote: Emote?,
    private val reactionString: String?,
) : ListenerAdapter() {
    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        super.onMessageReactionAdd(event)
        if (event.user!!.idLong != instance.gateway.selfUser.idLong) {
            if (event.messageId == message.id) {
                if (
                    reactionEmote == null && reactionString == null ||
                    reactionEmote != null && event.reaction.reactionEmote.isEmote && event.reaction.reactionEmote.emote == reactionEmote ||
                    reactionString != null && event.reaction.reactionEmote.isEmoji && event.reaction.reactionEmote.emoji == reactionString
                ) {
                    logic(event.user!!, params, event)
                }
            }
        }
    }
}
