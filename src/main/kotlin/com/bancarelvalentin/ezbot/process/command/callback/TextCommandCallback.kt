package com.bancarelvalentin.ezbot.process.command.callback

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import net.dv8tion.jda.api.EmbedBuilder

abstract class TextCommandCallback(val request: CommandRequest, val response: CommandResponse) {

    open val reply = response.embedOverride != null || response.title != null || response.message != null

    open fun call(): EmbedBuilder? {
        request.handler.logger.trace("Callback called (${this.javaClass.simpleName})", LogCategoriesEnum.GENERIC_COMMAND)
        return if (reply || request.forceReplyOnEmpty) builder() else null
    }

    open fun builder(): EmbedBuilder {
        return if (response.embedOverride != null) {
            response.embedOverride!!
        } else {
            val builder = EmbedBuilder()
                .setColor(response.color)
                .setTitle(response.defaultTitle)
                .setFooter(ConfigHandler.config.version ?: "")
            if (response.message != null) {
                builder.setDescription(response.message)
            }
            if (response.title != null) {
                builder.setTitle(response.title)
            }
            builder
        }
    }
}
