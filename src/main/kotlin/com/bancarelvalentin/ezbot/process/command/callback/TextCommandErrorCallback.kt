package com.bancarelvalentin.ezbot.process.command.callback

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.exception.implerrors.UnknownErrorWrapper
import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.exception.usererrors.ImplementationErrorWrapper
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.Constants
import com.bancarelvalentin.ezbot.utils.ExceptionUtils
import com.bancarelvalentin.ezbot.utils.UserUtils
import net.dv8tion.jda.api.EmbedBuilder

open class TextCommandErrorCallback(request: CommandRequest, response: CommandResponse, val exception: Exception) :
    TextCommandCallback(request, response) {

    override fun call(): EmbedBuilder? {
        val emote: String
        val builder: EmbedBuilder = super.call() ?: super.builder()

        val ex: AbstractUserException = if (exception is AbstractUserException) {
            request.handler.logger.debug("Text command user error  \t`$request`\t`$response`", LogCategoriesEnum.GENERIC_COMMAND)
            emote = Constants.EMOTE_WARNING.tounicode()
            builder.setColor(Constants.COLOR_WARNING)
            exception
        } else {
            request.handler.logger.error("Text command execution error  \t`$request`\t`$response`", LogCategoriesEnum.GENERIC_COMMAND, throwable = exception, user = request.user)
            emote = Constants.EMOTE_ERROR.tounicode()
            val stackAsString = ExceptionUtils.stackAsString(exception, 0, "com.bancarelvalentin")
            builder.setColor(Constants.COLOR_ERROR)
            if (UserUtils.isModerator(request.user, request.guild?.idLong)) {
                builder.setDescription(stackAsString.substring(0, minOf(stackAsString.length, 2000)))
            }
            val implError = if (exception is AbstractImplementationException) exception else UnknownErrorWrapper(exception)
            ImplementationErrorWrapper(implError)
        }
        builder.setTitle(ex.message)
        request.forceAddReaction(emote)
        return builder
    }
}
