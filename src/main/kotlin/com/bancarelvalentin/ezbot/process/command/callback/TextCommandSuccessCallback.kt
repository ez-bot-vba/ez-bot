package com.bancarelvalentin.ezbot.process.command.callback

import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import net.dv8tion.jda.api.EmbedBuilder

open class TextCommandSuccessCallback(request: CommandRequest, response: CommandResponse) : TextCommandCallback(request, response) {

    override fun call(): EmbedBuilder? {
        request.handler.logger.trace("Text command execution success \t`$request`\t`$response`", LogCategoriesEnum.GENERIC_COMMAND)
        request.forceAddReaction(response.emote)
        return super.call()
    }
}
