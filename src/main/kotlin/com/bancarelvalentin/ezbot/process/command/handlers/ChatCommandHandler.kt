package com.bancarelvalentin.ezbot.process.command.handlers

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.commands.UnknownChatCommandMessageType
import com.bancarelvalentin.ezbot.exception.usererrors.MissingRequiredParameterException
import com.bancarelvalentin.ezbot.exception.usererrors.WrongTypeParameterException
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.LogCategory
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.ExtraParam
import com.bancarelvalentin.ezbot.process.command.request.ChatCommandRequest
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.GenericMessageEvent
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.events.message.MessageUpdateEvent
import org.apache.tools.ant.BuildException
import org.apache.tools.ant.types.Commandline

open class ChatCommandHandler(val parsedChatCommands: Pair<String, String>, command: Command, val event: GenericMessageEvent) : AbstractCommandHandler(command) {

    private val message = when (event) {
        is MessageUpdateEvent -> event.message
        is MessageReceivedEvent -> event.message
        else -> throw UnknownChatCommandMessageType(event::class.java)
    }
    override val logCat: LogCategory = LogCategoriesEnum.CHAT_COMMAND

    override fun getGuild(): Guild {
        return event.guild
    }

    override fun getReplyChannel(): MessageChannel {
        return event.textChannel
    }

    override fun getUser(): User {
        return if (event is MessageUpdateEvent) event.author else if (event is MessageReceivedEvent) event.author else throw UnknownChatCommandMessageType(event.javaClass)
    }

    override fun getRequest(): CommandRequest? {
        val content = message.contentRaw
        val args = try {
            Commandline.translateCommandline(content)
        } catch (e: BuildException) {
            content.split(Regex("\\s+")).toTypedArray()
        }
        return ChatCommandRequest(command, message, parsedChatCommands.first, parsedChatCommands.second, args.copyOfRange(1, args.size))
    }

    override fun parseParams(request: CommandRequest): Array<CommandParam<out Any?>> {
        val args: Array<String> = request.rawParams.map { it as String }.toTypedArray()
        val params = ArrayList<CommandParam<out Any?>>()
        if (this.command.paramsClasses.isNotEmpty()) {
            val max = maxOf(this.command.paramsClasses.size, args.size)
            for (i in 0 until max) {
                val clazz: Class<out CommandParam<out Any?>> = try {
                    this.command.paramsClasses[i]
                } catch (e: IndexOutOfBoundsException) {
                    ExtraParam::class.java
                }

                val arg = try {
                    args[i]
                } catch (e: IndexOutOfBoundsException) {
                    null
                }

                val paramInstance = clazz.getDeclaredConstructor().newInstance()
                if (arg != null) {
                    try {
                        paramInstance.setRaw(arg, gateway)
                    } catch (e: Exception) {
                        throw WrongTypeParameterException(paramInstance.rawName, paramInstance.docType, i + 1)
                    }
                }
                if (paramInstance.get() == null && !paramInstance.optional) {
                    throw MissingRequiredParameterException(paramInstance.rawName, i + 1)
                } else {
                    params.add(paramInstance)
                }
            }
        }
        return params.toTypedArray()
    }

    companion object {
        fun potentialCommand(message: Message): Pair<String, String>? {
            return try {
                if (message.mentionedUsers.map { it.idLong }.contains(Orchestrator.gateway.selfUser.idLong)) { // On mention
                    Pair(Orchestrator.gateway.selfUser.asMention, message.contentRaw.split("\\s")[1])
                } else { // On raw chat
                    val prefix = parseChatCommandPrefix(message.contentRaw) ?: return null
                    Pair(prefix, message.contentRaw.split(Regex("\\s"))[0].substring(prefix.length))
                }
            } catch (e: IndexOutOfBoundsException) { // In case of no content after the prefix
                null
            }
        }

        private fun parseChatCommandPrefix(message: String): String? {
            ConfigHandler.config.prefixes.forEach { p ->
                if (message.startsWith(p, true)) {
                    return p
                }
            }
            return null
        }
    }
}
