package com.bancarelvalentin.ezbot.process.command.handlers

import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.LogCategory
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.request.LazyCommandRequest
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent
import java.util.regex.Pattern

class LazyCommandHandler(command: Command, val event: MessageReactionAddEvent) : AbstractCommandHandler(command) {

    override val logCat: LogCategory = LogCategoriesEnum.LAZY_COMMAND
    private val lazyCommandsChannel = getUser().openPrivateChannel().complete()

    override fun getGuild(): Guild {
        return event.guild
    }

    override fun getReplyChannel(): MessageChannel {
        return lazyCommandsChannel
    }

    override fun getUser(): User {
        return event.user!!
    }

    override fun getRequest(): CommandRequest? {
        val embeds = event.retrieveMessage().complete().embeds
        if (embeds.isEmpty()) return null
        if (this.command.rawName != (embeds[0].title ?: "").split(Pattern.compile("\\W"))[0]) return null

        return LazyCommandRequest(command, getReplyChannel(), event, command.patterns[0])
    }

    override fun parseParams(request: CommandRequest): Array<CommandParam<out Any?>> {
        val params = ArrayList<CommandParam<out Any?>>()
        if (this.command.paramsClasses.isNotEmpty()) {
            for (i in 0 until this.command.paramsClasses.size) {
                val clazz: Class<out CommandParam<out Any?>> = this.command.paramsClasses[i]

                val paramInstance = clazz.getDeclaredConstructor().newInstance()

                if (paramInstance.get() == null && !paramInstance.optional) {
                    logger.warn("Missing required param; those are not implemented yet")
                }
                params.add(paramInstance)
            }
        }
        return params.toTypedArray()
    }
}
