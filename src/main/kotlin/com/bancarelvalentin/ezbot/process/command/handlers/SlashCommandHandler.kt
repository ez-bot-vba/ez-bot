package com.bancarelvalentin.ezbot.process.command.handlers

import com.bancarelvalentin.ezbot.exception.implerrors.commands.MultiplePotentialParamMatchesInASlashcommandException
import com.bancarelvalentin.ezbot.exception.implerrors.commands.UnknownParameterTypeInSlashCommandException
import com.bancarelvalentin.ezbot.exception.implerrors.commands.UnknownSlashCommandChannelTypeException
import com.bancarelvalentin.ezbot.exception.usererrors.MissingRequiredParameterException
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.LogCategory
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.BooleanCommandParam
import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.IntCommandParam
import com.bancarelvalentin.ezbot.process.command.param.NumberCommandParam
import com.bancarelvalentin.ezbot.process.command.param.RoleCommandParam
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.ezbot.process.command.param.UserCommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.request.SlashCommandRequest
import com.bancarelvalentin.ezbot.utils.SlashCommandRegistrationUtils
import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType

open class SlashCommandHandler(command: Command, val event: SlashCommandEvent) : AbstractCommandHandler(command) {
    override val logCat: LogCategory = LogCategoriesEnum.SLASH_COMMAND

    override fun getGuild(): Guild {
        return event.guild!!
    }

    override fun getReplyChannel(): MessageChannel {
        return event.textChannel
    }

    override fun getUser(): User {
        return event.user
    }

    override fun getRequest(): CommandRequest? {
        val name = event.name
        val formatSlashCommandDocTitle = SlashCommandRegistrationUtils.formatSlashCommandDocTitle(command.patterns[0])
        val match = name == formatSlashCommandDocTitle

        return if (match) {
            val channel: MessageChannel = when (event.channelType) {
                ChannelType.PRIVATE -> gateway.getPrivateChannelById(getReplyChannel().idLong)!!
                ChannelType.TEXT -> gateway.getTextChannelById(getReplyChannel().idLong)!!
                else -> throw UnknownSlashCommandChannelTypeException(event.channelType)
            }
            SlashCommandRequest(command, channel, event, event.name, event.options.toTypedArray())
        } else {
            null
        }
    }

    override fun parseParams(request: CommandRequest): Array<CommandParam<out Any?>> {
        val args: Array<OptionMapping> = request.rawParams.map { it as OptionMapping }.toTypedArray()

        val params = ArrayList<CommandParam<out Any?>>()
        for (clazz in command.paramsClasses) {
            val paramInstance = clazz.getDeclaredConstructor().newInstance()
            @Suppress("UNCHECKED_CAST")
            paramInstance as CommandParam<out Any>?

            val matchingOptions = args
                .filter { paramInstance.type == it.type }
                .filter { it.name == SlashCommandRegistrationUtils.formatSlashCommandDocTitle(paramInstance.rawName) }

            if (matchingOptions.size > 1) throw MultiplePotentialParamMatchesInASlashcommandException()
            if (matchingOptions.isNotEmpty()) {
                val option = matchingOptions[0]
                when (option.type) {
                    OptionType.STRING -> (paramInstance as StringCommandParam).set(option.asString)
                    OptionType.INTEGER -> (paramInstance as IntCommandParam).set(option.asDouble.toLong())
                    OptionType.NUMBER -> (paramInstance as NumberCommandParam).set(option.asDouble)
                    OptionType.BOOLEAN -> (paramInstance as BooleanCommandParam).set(option.asBoolean)
                    OptionType.CHANNEL -> (paramInstance as ChannelCommandParam).set(option.asMessageChannel)
                    OptionType.USER -> (paramInstance as UserCommandParam).set(option.asUser)
                    OptionType.ROLE -> (paramInstance as RoleCommandParam).set(option.asRole)
                    else -> throw UnknownParameterTypeInSlashCommandException(option.type)
                }
            }
            if (paramInstance.get() == null && !paramInstance.optional) {
                throw MissingRequiredParameterException(event.name, 0)
            } else {
                params.add(paramInstance)
            }
        }
        return params.toTypedArray()
    }
}
