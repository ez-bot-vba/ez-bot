package com.bancarelvalentin.ezbot.process.command.param

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.interactions.commands.OptionType

abstract class StringCommandParam : CommandParam<String>(), SupportAutocomplete {

    override val type = OptionType.STRING
    override val docType = Localizator.get(DefaultI18n.COMMAND_DOC_PARAM_TYPE_STRING)

    override fun setRaw(value: String?, gateway: JDA) {
        this.value = value
    }
}
