package com.bancarelvalentin.ezbot.process.command.param

import net.dv8tion.jda.api.interactions.commands.Command

interface SupportAutocomplete {
    val choices: List<Command.Choice>
        get() = ArrayList()
}
