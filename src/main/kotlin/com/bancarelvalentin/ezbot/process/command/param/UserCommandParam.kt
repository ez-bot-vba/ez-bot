package com.bancarelvalentin.ezbot.process.command.param

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.interactions.commands.OptionType

abstract class UserCommandParam : CommandParam<User>() {

    override val type = OptionType.USER
    override val docType = Localizator.get(DefaultI18n.COMMAND_DOC_PARAM_TYPE_USER)

    override fun stringify(): String {
        return get()?.asMention?.replace(">", "/${get()!!.name}>") ?: "null"
    }

    override fun setRaw(value: String?, gateway: JDA) {
        val id = value?.substring(2, value.length - 1)
        this.value = if (id != null) gateway.retrieveUserById(id).complete() else null
    }

    fun asMember(guild: Guild): Member? {
        return if (this.value != null) guild.retrieveMember(this.value!!).complete() else null
    }
}
