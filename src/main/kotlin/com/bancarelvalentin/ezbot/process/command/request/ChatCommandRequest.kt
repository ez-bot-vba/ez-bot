package com.bancarelvalentin.ezbot.process.command.request

import com.bancarelvalentin.ezbot.process.command.Command
import net.dv8tion.jda.api.entities.Emote
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed

@Suppress("unused")
class ChatCommandRequest(handler: Command, val event: Message, override val prefix: String, command: String, rawParams: Array<out Any>) :
    CommandRequest(handler, event.guild, event.channel, event.author, command, rawParams) {

    override fun forceReply(str: String) {
        this.event.reply(str).queue { reply = it }
    }

    override fun forceReply(str: Message) {
        this.event.reply(str).queue { reply = it }
    }

    override fun forceReply(embed: MessageEmbed) {
        this.event.replyEmbeds(embed).queue { reply = it }
    }

    override fun forceAddReaction(emote: Emote) {
        this.event.addReaction(emote).queue()
    }

    override fun forceAddReaction(emote: String) {
        this.event.addReaction(emote).queue()
    }
}
