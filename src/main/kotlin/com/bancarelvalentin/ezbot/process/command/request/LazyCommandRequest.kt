package com.bancarelvalentin.ezbot.process.command.request

import com.bancarelvalentin.ezbot.process.command.Command
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

@Suppress("unused")
class LazyCommandRequest(handler: Command, channel: MessageChannel, val event: MessageReactionAddEvent, command: String) :
    CommandRequest(handler, event.guild, channel, event.user!!, command, emptyArray()) {

    override val prefix = "/"

    override fun forceReply(str: String) {
        this.user.openPrivateChannel().queue { it.sendMessage(str).queue { reply = it } }
    }

    override fun forceReply(str: Message) {
        this.user.openPrivateChannel().queue { it.sendMessage(str).queue { reply = it } }
    }

    override fun forceReply(embed: MessageEmbed) {
        this.user.openPrivateChannel().queue { it.sendMessageEmbeds(embed).queue { reply = it } }
    }
}
