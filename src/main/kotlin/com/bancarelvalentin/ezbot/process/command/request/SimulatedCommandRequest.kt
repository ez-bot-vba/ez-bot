package com.bancarelvalentin.ezbot.process.command.request

import com.bancarelvalentin.ezbot.process.command.Command
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User

@Suppress("unused")
class SimulatedCommandRequest(command: Command, guild: Guild?, channel: TextChannel, user: User, cmd: String, params: Array<out Any>, override val prefix: String) : CommandRequest(command, guild, channel, user, cmd, params) {
    override fun forceReply(str: String) {}

    override fun forceReply(str: Message) {}

    override fun forceReply(embed: MessageEmbed) {}
}
