package com.bancarelvalentin.ezbot.process.doc

interface CommandDoc : Doc {

    /**
     * A sample command to be shown in documentation
     */
    val sample: String
        get() = getRawTriggers()[0]

    fun getRawTriggers(): List<String>
    fun generateDocTriggers(): String {
        return getRawTriggers().joinToString("`, `", "`", "`") { it }
    }

    override fun generateDocName(): String {
        val ignoreTriggers = getRawTriggers().joinToString { it }.equals(rawName, true)
        return "$rawName${if (!ignoreTriggers) " (${generateDocTriggers()})" else ""}"
    }
}
