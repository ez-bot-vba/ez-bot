package com.bancarelvalentin.ezbot.utils

import com.coreoz.wisp.Scheduler
import com.coreoz.wisp.schedule.FixedDelaySchedule
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date
import java.util.Hashtable
import java.util.UUID


@Suppress("unused")
class AsyncUtils {
    companion object {

        private val buffered = Hashtable<UUID, String>()
        val scheduler = Scheduler()

        fun delay(duration: Duration, runnable: Runnable) {
            exec(runnable, duration, true)
        }

        fun interval(duration: Duration, runnable: Runnable) {
            exec(runnable, duration, false)
        }

        fun schedule(date: Date, runnable: Runnable) {
            val now = LocalDateTime.now()
            val scheduledDate = date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()
            exec(runnable, Duration.between(now, scheduledDate), true)
        }

        fun buffer(id: String, duration: Duration, runnable: Runnable) {
            val buffered = buffered.filterValues { it == id }.keys.stream().findFirst()
            val uuid = if (buffered.isPresent) {
                val uuid = buffered.get()
                val bufferedJob = scheduler.findJob(uuid.toString())
                if (bufferedJob.isPresent) scheduler.cancel(uuid.toString())
                uuid
            } else {
                val newUuid = UUID.randomUUID()
                this.buffered[newUuid] = id
                newUuid
            }
            exec(uuid, runnable, duration, true)
        }

        private fun exec(runnable: Runnable, duration: Duration, cancelAfterFirst: Boolean) {
            exec(UUID.randomUUID(), runnable, duration, cancelAfterFirst)
        }

        private fun exec(uuid: UUID, runnable: Runnable, duration: Duration, cancelAfterFirst: Boolean) {
            scheduler.schedule(uuid.toString(), {
                runnable.run()
                if (cancelAfterFirst) scheduler.cancel(uuid.toString())
                if (buffered.containsKey(uuid)) buffered.remove(uuid)
            }, FixedDelaySchedule(duration))
        }
    }
}
    