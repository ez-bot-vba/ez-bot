package com.bancarelvalentin.ezbot.utils

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.handlers.SimulatedCommandHandler

@Suppress("unused")
class CommandUtils {
    companion object {
        fun simulateCommand(clazz: Class<out Command>, params: Array<out String>) {
            val command = clazz.getDeclaredConstructor().newInstance()
            val conf = ConfigHandler.config
            val guild = Orchestrator.gateway.getGuildById(conf.defaultWhitelistedGuildIds[0])!!
            val channel = Orchestrator.gateway.getTextChannelById(conf.defaultWhitelistedChannelIds[0])!!
            val user = Orchestrator.gateway.getUserById(conf.moderatorsIds[0])!!

            SimulatedCommandHandler(command, guild, channel, user, params).handle()
        }
    }
}
