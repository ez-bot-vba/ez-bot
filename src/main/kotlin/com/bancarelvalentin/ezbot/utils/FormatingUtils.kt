package com.bancarelvalentin.ezbot.utils

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import java.util.Date

@Suppress("unused")
class FormatingUtils {
    // Discord formating doc: https://discord.com/developers/docs/reference#message-formatting
    companion object {
        private fun format(prefix: String, id: String, suffix: String = "") = "<$prefix$id$suffix>"
        private fun format(prefix: String, id: Long, suffix: String = "") = format(prefix, id.toString(), suffix)

        fun formatUserId(userId: Long): String {
            return format("@", userId)
        }

        fun formatUserIdAsNickname(userId: Long): String {
            return format("@!", userId)
        }

        fun formatChannelId(channelId: Long): String {
            return format("#", channelId)
        }

        fun formatRoleId(roleId: Long): String {
            return format("@&", roleId)
        }

        fun formatCustomEmoji(emojiId: Long, emojiName: String, isAnimated: Boolean = false): String {
            val prefix = if (isAnimated) "a:" else ":"
            return format(prefix, "$emojiName:$emojiId")
        }

        fun getCustomEmoji(emojiName: String, guild: Guild): String {
            val optional = guild.emotes.stream().filter { it.name == emojiName }
                .findAny()
            return if (optional.isPresent) optional.get().asMention else ""
        }

        fun getCustomEmoji(emojiId: Long, guild: Guild): String {
            val optional = guild.emotes.stream()
                .filter { it.idLong == emojiId }
                .findAny()
            return if (optional.isPresent) optional.get().asMention else ""
        }

        fun formatDate(date: Date, format: TIMESTAMP_FORMAT = TIMESTAMP_FORMAT.LONG_DATE_TIME): String {
            return format("t:", date.toInstant().epochSecond.toString(), ":$format")
        }

        fun messageToLink(message: Message): String {
            return "https://discord.com/channels/${message.guild.id}/${message.channel.id}/${message.id}"
        }

        fun getRandomEmojies(count: Int = 1): List<EmojiEnum> {
            val list = ArrayList<EmojiEnum>()
            do {
                val new = EmojiEnum.values().random()
                if (list.stream().noneMatch { it.name == new.name } &&
                    new.tounicode().length <= 2
                ) {
                    list.add(new)
                }
            } while (list.size != count)
            return list
        }

        fun limit1024(str: String): String {
            return str.substring(0, minOf(1024, str.length))
        }
    }

    @Suppress("ClassName")
    enum class TIMESTAMP_FORMAT(private val format: String) {
        SHORT_TIME("t"),
        LONG_TIME("T"),
        SHORT_DATE("d"),
        LONG_DATE("D"),
        SHORT_DATE_TIME("f *"),
        LONG_DATE_TIME("F"),
        RELATIVE_TIME("R");

        override fun toString(): String {
            return format
        }
    }
}
