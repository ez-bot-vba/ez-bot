package com.bancarelvalentin.ezbot.utils

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import net.dv8tion.jda.api.entities.User

class UserUtils {
    companion object {
        fun isModerator(user: User, guildId: Long? = null): Boolean {
            if (ConfigHandler.config.moderatorsIds.contains(user.idLong)) return true
            if (guildId != null) {
                val member = Orchestrator.gateway.getGuildById(guildId)!!.retrieveMember(user).complete()
                if (member.roles.any { ConfigHandler.config.moderatorsRolesIds.contains(it.idLong) }) return true
            }
            return false
        }
    }
}
