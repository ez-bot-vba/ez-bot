package com.bancarelvalentin.ezbot.testbot

import com.fasterxml.jackson.annotation.JsonProperty

@Suppress("unused")
class DeepLiveConfig {

    @JsonProperty("testDeep")
    var testDeep: String? = null
}
