package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class DummyImplementationException(message: String? = null) : AbstractImplementationException(message ?: "Dummy exception")
