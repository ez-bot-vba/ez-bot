package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.config.Config
import java.awt.Color
import java.util.Locale

open class NoTestConfig : Config {
    override val prefixes: Array<String> = arrayOf("!")
    override val defaultColor: Color = Color.MAGENTA
    override val moderatorsIds = arrayOf(443750643375800320)
    override val supportSlashCommands = true
    override val createI18nOnBoot = true
    override val errorLogsChannelId = 954473682581991454
    override val supportLazyCommands = true
    override val lazyCommandsChannelId = 949051944553574510
    override val supportDjDedicatedChannel = true
    override val supportDjPlayerViaCommands = true
    override val djDedicatedChannelId = 955180385380225054
    override val defaultWhitelistedGuildIds = arrayOf(677258457778356237)
    override val language: Locale = Locale.FRANCE
    override val defaultWhitelistedChannelIds = arrayOf(954473626311204904, 954473565447651338)
    override val logErrorsOnSentry = true
    override val timeStartupInSentry = true
    override val timeCommandExecutionsInSentry = true
    override val timeCronExecutionsInSentry = true
    override val versionNumber: String = "69.420.test"
    override val liveConfigChannelId = 1008877654033829899
}
