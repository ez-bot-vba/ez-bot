package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.process.background.BackgroundProcess

abstract class TestBackgroundProcess : BackgroundProcess(), TestInstance
