package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.default.features.dj.DjEmoteAction
import com.bancarelvalentin.ezbot.utils.DjUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import net.dv8tion.jda.api.JDA

class TestConfig : NoTestConfig() {

    val dummyroleId = 904125291717357639
    val dummyChannelId = 954473486494093313
    val dummyUserId = 443750643375800320
    val dummyEmojiId = 983774674238443550
    val channelIdToExecuteTest = 983774776373968966

    override val supportSlashCommands = false
    override val defaultWhitelistedChannelIds = arrayOf(*super.defaultWhitelistedChannelIds, channelIdToExecuteTest)

    override val djDedicatedChannelExtraActionsEmotes = arrayOf(
        DjEmoteAction(EmojiEnum.KEYCAP_1, "Test dummy playlist YT") { user, _, event ->
            DjUtils.queueShortcut(event, user, "https://www.youtube.com/watch?v=NiX00UzUuLw&list=RDNiX00UzUuLw")
        },
        DjEmoteAction(EmojiEnum.KEYCAP_2, "Test dummy playlist Bandcamp") { user, _, event ->
            DjUtils.queueShortcut(event, user, "https://nowyswiat.bandcamp.com/album/the-story-of-nowy-wiat-vol-1?from=salesfeed")
        },
        DjEmoteAction(EmojiEnum.KEYCAP_3, "Test dummy video Vimeo") { user, _, event ->
            DjUtils.queueShortcut(event, user, "https://vimeo.com/369118631")
        },
        DjEmoteAction(EmojiEnum.KEYCAP_4, "Test dummy twitch :no_entry_sign: ") { user, _, event ->
            DjUtils.queueShortcut(event, user, "https://www.twitch.tv/mostlymxc")
        },
        DjEmoteAction(EmojiEnum.KEYCAP_5, "Test dummy playlist soundcloud :no_entry_sign: ") { user, _, event ->
            DjUtils.queueShortcut(event, user, "https://soundcloud.com/marion-gil-2/sets/hardcore")
        }
    )

    val channelIdToEmpty = arrayOf(
        dummyChannelId, // general
        channelIdToExecuteTest, // cmd
        954473565447651338, // cmd-admin
        errorLogsChannelId, // error-logs
    )

    override val extraOnReadyListeners: Array<(JDA) -> Unit>
        get() = arrayOf(
            { it.openPrivateChannelById(dummyUserId).queue { channel -> channel.sendMessage("extraOnReadyListeners working").queue() } }
        )
}
