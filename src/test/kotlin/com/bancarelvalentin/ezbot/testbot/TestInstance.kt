package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.process.Instance

interface TestInstance : Instance, TestDoc
