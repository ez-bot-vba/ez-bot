package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.config.LiveConfig
import com.fasterxml.jackson.annotation.JsonProperty

@Suppress("unused")
class TestLiveConfig : LiveConfig {

    @JsonProperty("test")
    var test: String = "default"

    @JsonProperty("deep")
    var deep: DeepLiveConfig = DeepLiveConfig()
}
