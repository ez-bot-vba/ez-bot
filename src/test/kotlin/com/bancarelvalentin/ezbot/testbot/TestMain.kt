package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler

// https://discord.com/oauth2/authorize?client_id=777511920727097414&scope=bot&permissions=8
// https://stackoverflow.com/questions/68292025/how-to-use-slash-command-in-channel-with-jda
class TestMain {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            ConfigHandler.config = TestConfig()
            ConfigHandler.liveConfig = TestLiveConfig()
            if (System.getenv("TEST")?.equals("true") == true) {
                ConfigHandler.config = TestConfig()
                Orchestrator.add(*TestOrchestrator.all)
            } else {
                ConfigHandler.config = NoTestConfig()
            }
            Orchestrator.start()
        }
    }
}
