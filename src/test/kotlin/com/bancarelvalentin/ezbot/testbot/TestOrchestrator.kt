package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.utils.ReflectionUtils
import java.lang.reflect.Modifier
import java.util.stream.Collectors
import java.util.stream.Stream

class TestOrchestrator {
    companion object {
        val all = all()

        private fun all(): Array<out TestInstance> {
            val classes: Stream<Class<out TestInstance>> = ReflectionUtils.getAllClassesOfType()
            return classes
                .distinct()
                .filter { !it.isInterface && it.declaredConstructors.isNotEmpty() && !Modifier.isAbstract(it.modifiers) }
                .map { it.getDeclaredConstructor().newInstance() }
                .collect(Collectors.toList()).toTypedArray()
        }
    }
}
