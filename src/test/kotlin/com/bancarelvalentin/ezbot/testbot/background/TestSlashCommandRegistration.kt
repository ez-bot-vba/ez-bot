package com.bancarelvalentin.ezbot.testbot.background

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.testbot.TestBackgroundProcess
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.interactions.commands.OptionType

@Suppress("unused")
class TestSlashCommandRegistration : TestBackgroundProcess() {

    override fun getListeners(): Array<EventListener> {
        return arrayOf(TestMsgOnReadyProcessListener(this))
    }

    override val rawName: String = "Test slash command"
    override val rawDesc: String = "Register two dummy slash command one with no parameter; one with a parameter"

    class TestMsgOnReadyProcessListener(process: Process) : EventListener(process) {
        override fun onReady(event: ReadyEvent) {
            var command = gateway.upsertCommand("dummy", "---")
            command.queue()

            command = gateway.upsertCommand("dummy-with-param", "---")
            command.addOption(OptionType.STRING, "dummy-param", "---")
            command.queue()
        }
    }
}
