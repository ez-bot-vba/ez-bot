package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import com.bancarelvalentin.ezbot.testbot.TestUtils
import net.dv8tion.jda.api.EmbedBuilder
import java.util.function.BiConsumer
import java.util.stream.Collectors

@Suppress("unused")
class PrintAllRawCommandsCommand : TestCommand() {

    override val patterns = arrayOf("print_all", "pa")

    override val logic = BiConsumer { _: CommandRequest, _: CommandResponse ->
        var desc = ""
        desc += TestUtils.allTestComands().map { testProcess ->
            testProcess.commandsToRun()
                .toList().stream()
                .map { "`${ConfigHandler.config.prefixes[0]}${it.syntax}`" }
                .collect(Collectors.joining("\n"))
        }
            .collect(Collectors.joining("\n"))
        TestUtils.sendInAllDefaultChannels(gateway, EmbedBuilder().setDescription(desc).build())
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("pa", STATUS.SUCCESS, "In #general; print all commands needed for a full test")
        )
    }
}
