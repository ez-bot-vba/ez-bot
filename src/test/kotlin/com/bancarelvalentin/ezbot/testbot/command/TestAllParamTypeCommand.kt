package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import com.bancarelvalentin.ezbot.testbot.TestConfig
import com.bancarelvalentin.ezbot.testbot.command.params.TestChannelRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestIntRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestNumberRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestRoleRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestStringRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestUserRequiredParam
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import net.dv8tion.jda.api.EmbedBuilder
import java.util.function.BiConsumer

@Suppress("unused")
class TestAllParamTypeCommand : TestCommand() {

    override val patterns = arrayOf("test_params", "tp")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(
        TestStringRequiredParam::class.java,
        TestIntRequiredParam::class.java,
        TestNumberRequiredParam::class.java,
        TestNumberRequiredParam::class.java,
        TestRoleRequiredParam::class.java,
        TestChannelRequiredParam::class.java,
        TestUserRequiredParam::class.java,
    )

    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->

        response.embedOverride = EmbedBuilder()
            .addField("String", request.getParamString(0), false)
            .addField("Int", request.getParamInt(1).toString(), false)
            .addField("Number", request.getParamNumber(2).toString(), false)
            .addField("Math", request.getParamNumber(3).toString(), false)
            .addField("Role", request.getParamRole(4)!!.asMention, false)
            .addField("Channel", request.getParamChannel(5)!!.asMention, false)
            .addField("User", "${request.getParamUser(6)!!.asMention} - ${request.getParamMember(6, request.guild!!)!!.asMention}", false)
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("tp Test 420", STATUS.WARNING, "React to command as warning and reply with a warning you missed a parameter"),
            AutoTest(
                "tp Test 420 6,9 69/420 ${FormatingUtils.formatRoleId((ConfigHandler.config as TestConfig).dummyroleId)} ${FormatingUtils.formatChannelId((ConfigHandler.config as TestConfig).dummyChannelId)} ${FormatingUtils.formatUserId((ConfigHandler.config as TestConfig).dummyUserId)}",
                STATUS.SUCCESS,
                "Return all passed parameters after being parsed to their corresponding type"
            )
        )
    }
}
