package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import com.bancarelvalentin.ezbot.testbot.command.params.TestChannelParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestChannelRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestIntParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestIntRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestNumberParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestNumberRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestRoleParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestRoleRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestStringParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestStringRequiredParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestUserParam
import com.bancarelvalentin.ezbot.testbot.command.params.TestUserRequiredParam
import java.util.function.BiConsumer

@Suppress("unused")
class TestCommandDocCommand : TestCommand() {

    override val whitelistChannelIds = arrayOf(677258458491519012)
    override val whitelistRoleIds = arrayOf(896780466613522482)
    override val whitelistUserIds = arrayOf(443750643375800320)
    override val patterns = arrayOf("test_doc", "td")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(
        TestStringRequiredParam::class.java,
        TestIntRequiredParam::class.java,
        TestNumberRequiredParam::class.java,
        TestRoleRequiredParam::class.java,
        TestChannelRequiredParam::class.java,
        TestUserRequiredParam::class.java,
        TestStringParam::class.java,
        TestStringParam::class.java,
        TestIntParam::class.java,
        TestNumberParam::class.java,
        TestRoleParam::class.java,
        TestChannelParam::class.java,
        TestUserParam::class.java,
    )

    override val logic = BiConsumer { _: CommandRequest, response: CommandResponse ->
        response.responseStatus = STATUS.WARNING
        response.message = "Dummy test command. Please call ${commandsToRun()[0].syntax}"
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("h td", STATUS.SUCCESS, "Affiche un message de doc avec un paramètre requis et un optionel; au mois un paramètre de chaque type; un channel, un role et un user whitelisted"),
        )
    }
}
