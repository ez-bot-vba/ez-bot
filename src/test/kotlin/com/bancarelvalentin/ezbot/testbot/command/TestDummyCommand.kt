package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import java.util.function.BiConsumer

@Suppress("unused")
class TestDummyCommand : TestCommand() {

    override val patterns = arrayOf("ping", "pong")

    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        when (request.command) {
            "ping" -> request.forceReply("pong")
        }
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("ping", STATUS.SUCCESS, "return pong"),
        )
    }
}
