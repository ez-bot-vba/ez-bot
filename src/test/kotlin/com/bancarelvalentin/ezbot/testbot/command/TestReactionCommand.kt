package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.request.ChatCommandRequest
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import java.util.function.BiConsumer

@Suppress("unused")
class TestReactionCommand : TestCommand() {

    override val patterns = arrayOf("test_reaction", "tr")

    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        if (request is ChatCommandRequest) {
            request.event.reply("React to this to test that I send another successs message").queue {
                it.addReaction(EmojiEnum.CHECK_MARK.tounicode()).queue()
                addReactionEvent(it, EmojiEnum.CHECK_MARK, null) { member, _, _ ->
                    member.openPrivateChannel().queue { channel ->
                        channel.sendMessage(
                            "${this.javaClass.simpleName} is a success"
                        ).queue()
                    }
                }
            }
        } else {
            request.forceReply("Only available via chat command ${getSyntaxExample()}")
        }
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("tr", STATUS.SUCCESS, "Should send a message and react to it with an emote. If you click on the emote it should send a private success message"),
        )
    }
}
