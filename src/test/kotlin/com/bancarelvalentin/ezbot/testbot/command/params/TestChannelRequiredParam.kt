package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam

open class TestChannelRequiredParam : ChannelCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
}
