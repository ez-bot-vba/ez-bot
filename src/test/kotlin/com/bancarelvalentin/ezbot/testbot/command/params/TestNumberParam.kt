package com.bancarelvalentin.ezbot.testbot.command.params

class TestNumberParam : TestIntRequiredParam() {
    override val optional = true
}
