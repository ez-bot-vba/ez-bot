package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.NumberCommandParam

open class TestNumberRequiredParam : NumberCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
}
