package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam

open class TestStringRequiredParam : StringCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
}
