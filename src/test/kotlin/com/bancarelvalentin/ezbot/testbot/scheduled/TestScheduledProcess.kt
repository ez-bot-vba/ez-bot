package com.bancarelvalentin.ezbot.testbot.scheduled

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.testbot.TestConfig
import com.bancarelvalentin.ezbot.testbot.TestScheduledProcess

@Suppress("unused")
class TestScheduledProcess : TestScheduledProcess() {

    override val rawName: String = "Dummy schedule"
    override val rawDesc: String = "Set presence to time including seconds on a recurring schedule"

    override val cron = "0 * * * * ? *"

    override fun logic() {
        gateway.openPrivateChannelById((ConfigHandler.config as TestConfig).dummyUserId).queue { it.sendMessage("Test CRON executed").queue() }
        this.logger.debug("Stopping the test cron after it's first execution to avoid log spam")
        this.cancel()
    }
}
